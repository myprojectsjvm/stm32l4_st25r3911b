/***************************************************************************//**
	@file		nfcv.c
	@brief		NFC-V commands.
********************************************************************************

	This driver is used to search for tags compliance with ISO15693.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		April-2021.
*******************************************************************************/

#include "nfcv.h"

#include "rfal_nfcv.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

static void Nfcv_Handle_Standby(nfcv_handle_t *h);
static void Nfcv_Handle_Field_On(nfcv_handle_t *h);
static void Nfcv_Handle_Field_On(nfcv_handle_t *h);
static void Nfcv_Handle_Init(nfcv_handle_t *h);
static void Nfcv_Handle_Guard_Time(nfcv_handle_t *h);
static void Nfcv_Handle_Inventory(nfcv_handle_t *h);
static void Nfcv_Handle_Wait(nfcv_handle_t *h);
static void Nfcv_Handle_Deinit(nfcv_handle_t *h);
static void Nfcv_Handle_Error(nfcv_handle_t *h);

/***************************************************************************//**
	@brief		Wait to start.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfcv_Handle_Standby(nfcv_handle_t *h) {

	if (h->control == NFCV_CONTROL_START) {
		h->control = NFCV_CONTROL_NONE;
		h->state = NFCV_HANDLE_STATE_FIELD_ON;
	}

}

/***************************************************************************//**
	@brief		Turn on the field.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfcv_Handle_Field_On(nfcv_handle_t *h) {

	rfalSetGT(RFAL_TIMING_NONE);
	if (rfalFieldOnAndStartGT() != ERR_NONE) {
		h->reg.flags.error = 1;
		h->state = NFCV_HANDLE_STATE_WAIT;
	}
	else {
		h->state = NFCV_HANDLE_STATE_INIT;
	}

}

/***************************************************************************//**
	@brief		ISO14443A initialization.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfcv_Handle_Init(nfcv_handle_t *h) {

	if (iso15693Initialize(0, 0) != ERR_NONE) {
		h->reg.flags.error = 1;
		h->state = NFCV_HANDLE_STATE_WAIT;
	}
	else {
		h->state = NFCV_HANDLE_STATE_GUARD_TIME;
	}

}

/***************************************************************************//**
	@brief		Waits the minimum time after the field is turned on.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfcv_Handle_Guard_Time(nfcv_handle_t *h) {

	if (rfalIsGTExpired()) {
		h->state = NFCV_HANDLE_STATE_INVENTORY;
	}

}

/***************************************************************************//**
	@brief		Inventory request.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfcv_Handle_Inventory(nfcv_handle_t *h) {

	uint8_t err;

	err = iso15693Inventory(
		ISO15693_NUM_SLOTS_16, 0, NULL,
		h->cards, sizeof(h->cards)/sizeof(iso15693ProximityCard_t), &h->cardsFound
	);

	if ((err != ERR_NONE) && (err != ERR_NOTFOUND)) {
		h->reg.flags.error = 1;
	}

	h->state = NFCV_HANDLE_STATE_WAIT;

}

/***************************************************************************//**
	@brief		Wait to stop.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfcv_Handle_Wait(nfcv_handle_t *h) {

	if (h->control == NFCV_CONTROL_STOP) {
		h->control = NFCV_CONTROL_NONE;
		if (h->reg.flags.error) {
			h->reg.flags.error = 0;
			h->state = NFCV_HANDLE_STATE_ERROR;
		}
		else {
			h->state = NFCV_HANDLE_STATE_DEINIT;
		}
	}

}

/***************************************************************************//**
	@brief		Protocol deinitialization.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfcv_Handle_Deinit(nfcv_handle_t *h) {

	if (iso15693Deinitialize(0) != ERR_NONE) {
		h->state = NFCV_HANDLE_STATE_ERROR;
	}
	else {
		h->state = NFCV_HANDLE_STATE_STANDBY;
	}

}

/***************************************************************************//**
	@brief		Error handling.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfcv_Handle_Error(nfcv_handle_t *h) {

	/* Try to turn off the field in case of error: */
	rfalFieldOff();

	/* Reset: */
	h->state = NFCV_HANDLE_STATE_STANDBY;

}

/***************************************************************************//**
	@brief		Driver initialization.

	Must be called in the application initialization.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfcv_Init(nfcv_handle_t *h) {

	h->reg.value = 0;
	h->state = NFCV_HANDLE_STATE_STANDBY;
	h->control = NFCV_CONTROL_NONE;

}

/***************************************************************************//**
	@brief		Driver handle.

	Must be called in the main loop.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfcv_Handle(nfcv_handle_t *h) {

	/* State machine routines: */
	static void (*const fnc[])(nfcv_handle_t *h) = {
		Nfcv_Handle_Standby,		/**< NFCV_HANDLE_STATE_STANDBY. */
		Nfcv_Handle_Field_On,		/**< NFCV_HANDLE_STATE_FIELD_ON. */
		Nfcv_Handle_Init,			/**< NFCV_HANDLE_STATE_INIT. */
		Nfcv_Handle_Guard_Time,		/**< NFCV_HANDLE_STATE_GUARD_TIME. */
		Nfcv_Handle_Inventory,		/**< NFCV_HANDLE_STATE_INVENTORY. */
		Nfcv_Handle_Wait,			/**< NFCV_HANDLE_STATE_WAIT. */
		Nfcv_Handle_Deinit,			/**< NFCV_HANDLE_STATE_DEINIT. */
		Nfcv_Handle_Error,			/**< NFCV_HANDLE_STATE_ERROR. */
	};

	/* State machine: */
	if (h->state < (sizeof(fnc)/sizeof(fnc[0]))) {
		fnc[h->state](h);
	}

}

//----------------------------------------------------------------------------//
