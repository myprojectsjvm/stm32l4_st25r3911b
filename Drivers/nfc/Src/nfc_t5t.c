/***************************************************************************//**
	@file		nfc_t5t.c
	@brief		NFC Type 5 Tag driver.
********************************************************************************

	This driver interfaces with the NFC layer to provide the identification,
	read and write in some tag compliance with NFC Type 5 Tag.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		A-2021.
*******************************************************************************/

#include "nfc_t5t.h"

#include "rfal_nfcv.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

static void Nfc_T5T_Handle_Start(nfc_t5t_handle_t *h);
static void Nfc_T5T_Handle_Wait(nfc_t5t_handle_t *h);
static void Nfc_T5T_Handle_Read_Block(nfc_t5t_handle_t *h);
static void Nfc_T5T_Handle_Write_Block(nfc_t5t_handle_t *h);
static void Nfc_T5T_Handle_Stop(nfc_t5t_handle_t *h);
static void Nfc_T5T_Handle_Error(nfc_t5t_handle_t *h);

/***************************************************************************//**
	@brief		Process start.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T5T_Handle_Start(nfc_t5t_handle_t *h) {

	/* Wait the ISO layer standby: */
	if (h->nfcv->state == NFCV_HANDLE_STATE_STANDBY) {
		/* Search start: */
		h->nfcv->control = NFCV_CONTROL_START;
		/* Wait the response: */
		h->state = NFC_T5T_HANDLE_STATE_WAIT;
	}

}

/***************************************************************************//**
	@brief		Wait the response.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T5T_Handle_Wait(nfc_t5t_handle_t *h) {

	/* Wait the ISO layer response: */
	if (h->nfcv->state == NFCV_HANDLE_STATE_WAIT) {
		if (h->nfcv->reg.flags.error) {
			h->state = NFC_T5T_HANDLE_STATE_ERROR;
		}
		else {
			/* Check if can be some NFC Type 2 Tag: */
			if (h->nfcv->cardsFound) {
				/* Identification report: */
				Nfc_Event_Callback(NFC_EVENT_IDENTIFICATION, NFC_TYPE_NFC_T5T, h);
				/* Read request: */
				if (h->reg.flags.rdBlockRqst) {
					h->state = NFC_T5T_HANDLE_STATE_READ_BLOCK;
				}
				/* Write request: */
				else if (h->reg.flags.wrBlockRqst) {
					h->state = NFC_T5T_HANDLE_STATE_WRITE_BLOCK;
				}
				/* So, finished: */
				else {
					h->state = NFC_T5T_HANDLE_STATE_STOP;
				}
			}
			else {
				Nfc_Event_Callback(NFC_EVENT_NOT_FOUND, NFC_TYPE_NFC_T5T, h);
				h->state = NFC_T5T_HANDLE_STATE_STOP;
			}
		}
	}

}

/***************************************************************************//**
	@brief		Block read.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T5T_Handle_Read_Block(nfc_t5t_handle_t *h) {

	uint8_t data[8];
	uint16_t len;
	ReturnCode err;

	err = rfalNfcvPollerReadSingleBlock(RFAL_NFCV_REQ_FLAG_DATA_RATE, h->nfcv->cards[0].uid, h->rdIdx, data, sizeof(data), &len);

	if ((err == ERR_NONE) && (len == 5)) {
		h->rdBlock[0] = data[1];
		h->rdBlock[1] = data[2];
		h->rdBlock[2] = data[3];
		h->rdBlock[3] = data[4];
		h->reg.flags.rdBlockRqst = 0;
		Nfc_Event_Callback(NFC_EVENT_READ, NFC_TYPE_NFC_T5T, h);
		h->state = NFC_T5T_HANDLE_STATE_STOP;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_READ_FAIL, NFC_TYPE_NFC_T5T, h);
		h->state = NFC_T5T_HANDLE_STATE_STOP;
	}

}

/***************************************************************************//**
	@brief		Block write.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T5T_Handle_Write_Block(nfc_t5t_handle_t *h) {

	ReturnCode err;

	err = rfalNfcvPollerWriteSingleBlock(RFAL_NFCV_REQ_FLAG_DATA_RATE, h->nfcv->cards[0].uid, h->wrIdx, h->wrBlock, sizeof( h->wrBlock));

	if (err == ERR_NONE) {
		h->reg.flags.wrBlockRqst = 0;
		Nfc_Event_Callback(NFC_EVENT_WRITE, NFC_TYPE_NFC_T5T, h);
		h->state = NFC_T5T_HANDLE_STATE_STOP;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_WRITE_FAIL, NFC_TYPE_NFC_T5T, h);
		h->state = NFC_T5T_HANDLE_STATE_STOP;
	}

}

/***************************************************************************//**
	@brief		Process stop.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T5T_Handle_Stop(nfc_t5t_handle_t *h) {

	/* Releases the ISO layer: */
	h->nfcv->control = NFCV_CONTROL_STOP;
	/* Releases the this layer: */
	h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	/* Resets the state machine: */
	h->state = NFC_T5T_HANDLE_STATE_START;

}

/***************************************************************************//**
	@brief		Error handling.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T5T_Handle_Error(nfc_t5t_handle_t *h) {

	/* Error report: */
	Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T5T, h);
	/* Releases the ISO layer: */
	h->nfcv->control = NFCV_CONTROL_STOP;
	/* Releases the this layer: */
	h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	/* Resets the state machine: */
	h->state = NFC_T5T_HANDLE_STATE_START;

}

/***************************************************************************//**
	@brief		Driver initialization.

	Must be called in the application initialization.

	@param		param	Driver handle structure.
	@param		iso		ISO handler.
	@return		None.
*******************************************************************************/

void Nfc_T5T_Init(void *param, void *iso) {

	nfc_t5t_handle_t *h = (nfc_t5t_handle_t *)param;

	h->reg.value = 0;

	h->state = NFC_T5T_HANDLE_STATE_START;

	h->protocolControl = NFC_PROTOCOL_CONTROL_READY;

	h->nfcv = (nfcv_handle_t *)iso;

}

/***************************************************************************//**
	@brief		Driver time base.

	Must be called in a 10ms routine.

	@param		param Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_T5T_Tick(void *param) {

}

/***************************************************************************//**
	@brief		Driver handle.

	Must be called in the main loop.

	@param		param Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_T5T_Handle(void *param) {

	nfc_t5t_handle_t *h = (nfc_t5t_handle_t *)param;

	/* State machine routines: */
	static void (*const fnc[])(nfc_t5t_handle_t *h) = {
		Nfc_T5T_Handle_Start,		/**< NFC_T5T_HANDLE_STATE_START. */
		Nfc_T5T_Handle_Wait,		/**< NFC_T5T_HANDLE_STATE_WAIT. */
		Nfc_T5T_Handle_Read_Block,	/**< NFC_T5T_HANDLE_STATE_READ_BLOCK. */
		Nfc_T5T_Handle_Write_Block,	/**< NFC_T5T_HANDLE_STATE_WRITE_BLOCK. */
		Nfc_T5T_Handle_Stop,		/**< NFC_T5T_HANDLE_STATE_STOP. */
		Nfc_T5T_Handle_Error,		/**< NFC_T5T_HANDLE_STATE_ERROR. */
	};

	/* State machine: */
	if (h->protocolControl != NFC_PROTOCOL_CONTROL_FINISHED) {
		if (h->state < (sizeof(fnc)/sizeof(fnc[0]))) {
			fnc[h->state](h);
		}
	}

}

/***************************************************************************//**
	@brief		Useful routine to set the protocol control.
	@param		control	Control used by upon layers to control the chip usage.
	@param		param 	Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_T5T_Control_Set(nfc_protocol_control_t control, void *param) {

	nfc_t5t_handle_t *h = (nfc_t5t_handle_t *)param;

	if (h->reg.flags.enable && (!h->reg.flags.block)) {
		/* Normal mode: */
		h->protocolControl = control;
	}
	else {
		/* Block mode: */
		h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	}


}

/***************************************************************************//**
	@brief		Useful routine to get the protocol control.
	@param		param 	Driver handle structure.
	@return		Control used by upon layers to control the chip usage.
*******************************************************************************/

nfc_protocol_control_t Nfc_T5T_Control_Get(void *param) {

	nfc_t5t_handle_t *h = (nfc_t5t_handle_t *)param;

	return h->protocolControl;

}

//----------------------------------------------------------------------------//
