/***************************************************************************//**
	@file		mifare_classic.c
	@brief		MIFARE Classic driver.
********************************************************************************

	This driver can be used to authenticate, read and write in the MIFARE
	Classic.

	The following times was measured in each state of the main state machine
	with core clock in 48MHz and the SPI clock in 3MHz:

	Mifare_Classic_Handle_Start				: 00.00ms;
	Mifare_Classic_Handle_Wait				: 00.00ms;
	Mifare_Classic_Handle_Auth				: 07.50ms;
	Mifare_Classic_Handle_Read				: 07.90ms;
	Mifare_Classic_Handle_Write				: 12.60ms;
	Mifare_Classic_Handle_Stop				: 00.00ms;

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		December-2020.
*******************************************************************************/

#include "mifare_classic.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/**
 * Default key.
 */
#define MIFARE_CLASSIC_DEFAULT_KEY		0xFFFFFFFFFFFF

/**
 * Default key type.
 */
#define MIFARE_CLASSIC_DEFAULT_TYPE		MIFARE_CLASSIC_KEY_A

/**
 * Default access block.
 */
#define MIFARE_CLASSIC_DEFAULT_BLOCK	0x04

/**
 * MIFARE Classic authentication flags.
 */
#define MIFARE_CLASSIC_FLAGS_AUTH_START (			\
		(uint32_t)RFAL_TXRX_FLAGS_CRC_TX_AUTO	|	\
		(uint32_t)RFAL_TXRX_FLAGS_CRC_RX_KEEP	|	\
		(uint32_t)RFAL_TXRX_FLAGS_PAR_TX_AUTO	|	\
		(uint32_t)RFAL_TXRX_FLAGS_PAR_RX_REMV	|	\
		(uint32_t)RFAL_TXRX_FLAGS_NFCIP1_OFF	|	\
		(uint32_t)RFAL_TXRX_FLAGS_AGC_ON		|	\
		(uint32_t)RFAL_TXRX_FLAGS_NFCV_FLAG_AUTO)

/**
 * MIFARE Classic authentication flags.
 */
#define MIFARE_CLASSIC_FLAGS_AUTH_READER (		 	\
		(uint32_t)RFAL_TXRX_FLAGS_CRC_TX_MANUAL	|	\
		(uint32_t)RFAL_TXRX_FLAGS_CRC_RX_KEEP	|	\
		(uint32_t)RFAL_TXRX_FLAGS_PAR_TX_NONE	|	\
		(uint32_t)RFAL_TXRX_FLAGS_PAR_RX_KEEP	|	\
		(uint32_t)RFAL_TXRX_FLAGS_NFCIP1_OFF	|	\
		(uint32_t)RFAL_TXRX_FLAGS_AGC_ON		|	\
		(uint32_t)RFAL_TXRX_FLAGS_NFCV_FLAG_AUTO)

/**
 * MIFARE Classic read flags.
 */
#define MIFARE_CLASSIC_FLAGS_READ (			 		\
		(uint32_t)RFAL_TXRX_FLAGS_CRC_TX_MANUAL	|	\
		(uint32_t)RFAL_TXRX_FLAGS_CRC_RX_KEEP	|	\
		(uint32_t)RFAL_TXRX_FLAGS_PAR_TX_NONE	|	\
		(uint32_t)RFAL_TXRX_FLAGS_PAR_RX_KEEP	|	\
		(uint32_t)RFAL_TXRX_FLAGS_NFCIP1_OFF	|	\
		(uint32_t)RFAL_TXRX_FLAGS_AGC_ON		|	\
		(uint32_t)RFAL_TXRX_FLAGS_NFCV_FLAG_AUTO)

/**
 * MIFARE Classic write flags.
 */
#define MIFARE_CLASSIC_FLAGS_WRITE (			 	\
		(uint32_t)RFAL_TXRX_FLAGS_CRC_TX_MANUAL	|	\
		(uint32_t)RFAL_TXRX_FLAGS_CRC_RX_KEEP	|	\
		(uint32_t)RFAL_TXRX_FLAGS_PAR_TX_NONE	|	\
		(uint32_t)RFAL_TXRX_FLAGS_PAR_RX_KEEP	|	\
		(uint32_t)RFAL_TXRX_FLAGS_NFCIP1_OFF	|	\
		(uint32_t)RFAL_TXRX_FLAGS_AGC_ON		|	\
		(uint32_t)RFAL_TXRX_FLAGS_NFCV_FLAG_AUTO)

/**
 * uint8_t vector to uint32_t value.
 */
#define _uint8_to_uint32(x, i)	(uint32_t)((x[i+0] << 24) | (x[i+1] << 16) | (x[i+2] << 8) | (x[i+3]))

/**
 * This 4 bytes number is used in the MIFARE authentication process and is
 * suppose to be a random number.
 */
#define MIFARE_CLASSIC_READER_NONCE				0x44332211

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

/*
 * General routines.
 */
static void Mifare_Classic_Crypto1_To_Rfal(const parity_data_t *src, uint8_t *dest, const uint32_t srcSize, const uint32_t destSize);
static void Mifare_Classic_Rfal_To_Crypto1(const uint8_t *src, parity_data_t *dest, const uint32_t size);
static uint16_t Mifare_Classic_CRC(const parity_data_t *data, const uint32_t len);
static void Mifare_Classic_Parity(parity_data_t *data, const uint32_t len);
static uint8_t Mifare_Classic_Check(parity_data_t *data, const uint32_t len);
ReturnCode Mifare_Classic_Auth(iso14443AProximityCard_t *card, crypto1_state *cryptoState, const uint8_t type, const uint64_t key, const uint8_t block);
ReturnCode Mifare_Classic_Read(crypto1_state *cryptoState, const uint8_t block, uint8_t *data);
ReturnCode Mifare_Classic_Write(crypto1_state *cryptoState, const uint8_t block, const uint8_t *data);

/*
 * Main state machine.
 */
static void Mifare_Classic_Handle_Start(mifare_classic_handle_t *h);
static void Mifare_Classic_Handle_Wait(mifare_classic_handle_t *h);
static void Mifare_Classic_Handle_Auth(mifare_classic_handle_t *h);
static void Mifare_Classic_Handle_Read(mifare_classic_handle_t *h);
static void Mifare_Classic_Handle_Write(mifare_classic_handle_t *h);
static void Mifare_Classic_Handle_Stop(mifare_classic_handle_t *h);
static void Mifare_Classic_Handle_Error(mifare_classic_handle_t *h);

/***************************************************************************//**
	@brief		Crypto1 frame to RFAL frame.

	Crypto1 data:

		-> 0x01F, 0x03E, 0x0AB, 0x1CF;
		-> 0 0001 1111, 0 0011 1110, 0 1010 1011, 1 1100 1111;

	RFAL data (the parity bit is concatenated in the LSB bit of the next byte):

		0x1F 0x7C 0xAC 0x7A 0x0E;

	@param		src			Crypto1 data (with parity).
	@param		dest		RFAL buffer.
	@param		srcSize		Source size.
	@param		destSize	Destination size.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Crypto1_To_Rfal(const parity_data_t *src, uint8_t *dest, const uint32_t srcSize, const uint32_t destSize) {

	uint8_t data, parity, bit;

	/* Initializations: */
	bit = 0;
	memset(dest, 0, destSize);
	/* Data conversion: */
	for (uint32_t i = 0, j = 0; i < srcSize; i++) {
		/* Getting data: */
		data = (uint8_t)src[i];
		parity = (uint8_t)(src[i] >> 8);
		/* First part of the data: */
		dest[j] |= data << bit;
		j++;
		/* Remaining bits: */
		if (bit) {
			dest[j] |=	data >> (8 - bit);
		}
		/* Parity bit: */
		dest[j] |= parity << bit++;
		if (bit == 8) {
			bit = 0;
			j++;
		}
	}

}

/***************************************************************************//**
	@brief		RFAL frame to Crypto1 frame.
	@param		src 	RFAL data (with parity).
	@param		dest	Crypto1 buffer.
	@param		size	Destination size.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Rfal_To_Crypto1(const uint8_t *src, parity_data_t *dest, const uint32_t size) {

	uint8_t data, parity, bit;

	/* Initializations: */
	bit = 0;
	/* Data conversion: */
	for (uint32_t i = 0, j = 0; i < size; i++) {
		/* First part of the data: */
		data = src[j] >> bit;
		j++;
		/* Remaining bits: */
		if (bit) {
			data |= src[j] << (8 - bit);
		}
		/* Parity bit: */
		if ((1 << bit++) & src[j]) {
			parity = 0x01;
		}
		else {
			parity = 0x00;
		}
		if (bit == 8) {
			bit = 0;
			j++;
		}
		/* Destination buffer: */
		dest[i] = (parity << 8) | data;
	}

}

/***************************************************************************//**
	@brief		Calculate CRC-A and return it.
	@param		data	Data pointer.
	@len		len		Data size.
	@return		CRC-A.
*******************************************************************************/

static uint16_t Mifare_Classic_CRC(const parity_data_t *data, const uint32_t len) {

	uint8_t byte;
	uint16_t crc;
	uint32_t i, j;

	/* Initializations: */
	crc = 0x6363;
	/* CRC calculation: */
	for (i = 0; i < len; i++) {
		byte = data[i] & 0xff;
		for (j = 0; j < 8; j++) {
			char bit = (crc ^ byte) & 1;
			byte >>= 1;
			crc >>= 1;
			if (bit) crc ^= 0x8408;
		}
	}

	return crc;

}

/***************************************************************************//**
	@brief		Calculate the parity.
	@param		data	Data pointer.
	@len		len		Data size.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Parity(parity_data_t *data, const uint32_t len) {

	uint32_t i;

	for (i = 0; i < len; i++) {
		data[i] = (data[i] & 0x00FF) | (ODD_PARITY(data[i]) << 8);
	}

}

/***************************************************************************//**
	@brief		Parity and CRC check.

	This routine execute the frame check, where the data comes with the
	parity and the CRC goes in the end of the vector.

	@param		data	Data pointer.
	@len		len		Data size.
	@return		TRUE to successful and FALSE to failure.
*******************************************************************************/

static uint8_t Mifare_Classic_Check(parity_data_t *data, const uint32_t len) {

	uint8_t ret = 1;
	uint16_t crcReceived, crcCalculed;
	uint32_t i;

	if (len > 2) {
		for (i = 0; i < len; i++) {
			if (ODD_PARITY(data[i] & 0x00FF) != (data[i] >> 8)) {
				ret = 0;
				break;
			}
		}
		if (ret) {
			crcReceived = ((data[len-1] & 0x00FF) << 8) | (data[len-2] & 0x00FF);
			crcCalculed = Mifare_Classic_CRC(data, len - 2);
			if (crcReceived != crcCalculed) {
				ret = 0;
			}
		}
	}
	else {
		ret = 0;
	}

	return ret;

}

/***************************************************************************//**
	@brief		MIFARE Classic authentication procedure.
	@param		card		ISO14443A card structure pointer.
	@param		cryptoState	Crypto1 state.
	@param		type		0x60 to KEYA and 0x61 to KEYB.
	@param		key			48 bits key.
	@param		block		Block address [0 to 63].
	@return		Driver return code.
*******************************************************************************/

ReturnCode Mifare_Classic_Auth(iso14443AProximityCard_t *card, crypto1_state *cryptoState, const uint8_t type, const uint64_t key, const uint8_t block) {

	ReturnCode ret;
	uint8_t param[9] = {0}, buffer[9] = {0};
	uint16_t actLen;
	parity_data_t reader_response[8];
	parity_data_t card_response[4];

	/* Getting card nonce: */
	param[0] = type;
	param[1] = block;
	ret = rfalTransceiveBlockingTxRx(param, 2, buffer, 4, &actLen, MIFARE_CLASSIC_FLAGS_AUTH_START, rfalConvMsTo1fc(50));
	if ((ret != ERR_CRC) || (actLen != 4)) {
		return ret = ERR_REQUEST;
	}

	/* Crypto1 initialization: */
	crypto1_new(cryptoState, 0, CRYPTO1_IMPLEMENTATION_CLEAN);

	/* First step (KEY + uid + tag nonce): */
	crypto1_init(cryptoState, key);
	crypto1_mutual_1(cryptoState, _uint8_to_uint32(card->uid, 0), _uint8_to_uint32(buffer, 0));

	/* Second step (reader nonce): */
	UINT32_TO_ARRAY_WITH_PARITY(MIFARE_CLASSIC_READER_NONCE, reader_response);
	if(!crypto1_mutual_2(cryptoState, reader_response)) {
		return ret = ERR_REQUEST;
	}
	Mifare_Classic_Crypto1_To_Rfal(reader_response, param, 8, 9);
	ret = rfalTransceiveBlockingTxRx(param, 9*8, buffer, 5, &actLen, MIFARE_CLASSIC_FLAGS_AUTH_READER, rfalConvMsTo1fc(50));
	if ((ret != ERR_INCOMPLETE_BYTE) || (actLen != 5)) {
		return ret = ERR_REQUEST;
	}

	/* Last step, card response validation: */
	Mifare_Classic_Rfal_To_Crypto1(buffer, card_response, 4);
	if(!crypto1_mutual_3(cryptoState, card_response)) {
		return ret = ERR_REQUEST;
	}

	return ret = ERR_NONE;

}

/***************************************************************************//**
	@brief		MIFARE Classic read procedure.
	@param		cryptoState	Crypto1 state.
	@param		block		Block address [0 to 63].
	@param		data		16 bytes buffer to store the block data.
	@return		Driver return code.
*******************************************************************************/

ReturnCode Mifare_Classic_Read(crypto1_state *cryptoState, const uint8_t block, uint8_t *data) {

	ReturnCode ret;
	parity_data_t pdTemp[18];
	uint8_t param[5], buffer[21] = {0x00};
	uint16_t crc, actLen;

	/* Getting the command with CRC: */
	pdTemp[0] = 0x30;
	pdTemp[1] = block;
	crc = Mifare_Classic_CRC(pdTemp, 2);
	pdTemp[2] = crc & 0x00FF;	/* LSB. */
	pdTemp[3] = crc >> 8;		/* MSB. */

	/* Parity: */
	Mifare_Classic_Parity(pdTemp, 4);

	/* Read command: */
	crypto1_transcrypt(cryptoState, pdTemp, 4);
	Mifare_Classic_Crypto1_To_Rfal(pdTemp, param, 4, 5);
	ret = rfalTransceiveBlockingTxRx(param, 4*8+4, buffer, 21, &actLen, MIFARE_CLASSIC_FLAGS_READ, rfalConvMsTo1fc(50));
	if ((ret != ERR_INCOMPLETE_BYTE) || (actLen != 21)) {
		return ret = ERR_REQUEST;
	}

	/* Decrypt: */
	Mifare_Classic_Rfal_To_Crypto1(buffer, pdTemp, 18);
	crypto1_transcrypt(cryptoState, pdTemp, 18);

	/* Parity and CRC check: */
	if (!Mifare_Classic_Check(pdTemp, 18)) {
		return ret = ERR_REQUEST;
	}

	/* Convert: */
	for (uint32_t i = 0; i < 16; i++) {
		data[i] = (uint8_t)pdTemp[i];
	}

	return ret = ERR_NONE;

}

/***************************************************************************//**
	@brief		MIFARE Classic write procedure.
	@param		cryptoState	Crypto1 state.
	@param		block		Block address [0 to 63].
	@param		data		16 bytes buffer to write in the block.
	@return		Driver return code.
*******************************************************************************/

ReturnCode Mifare_Classic_Write(crypto1_state *cryptoState, const uint8_t block, const uint8_t *data) {

	ReturnCode ret;
	parity_data_t pdTemp[21], ackTemp;
	uint8_t param[21], buffer[1] = {0x00};
	uint16_t crc, actLen;

	/* Getting the command with CRC: */
	pdTemp[0] = 0xA0;
	pdTemp[1] = block;
	crc = Mifare_Classic_CRC(pdTemp, 2);
	pdTemp[2] = crc & 0x00FF;	/* LSB. */
	pdTemp[3] = crc >> 8;		/* MSB. */

	/* Parity: */
	Mifare_Classic_Parity(pdTemp, 4);

	/* Write command: */
	crypto1_transcrypt(cryptoState, pdTemp, 4);
	Mifare_Classic_Crypto1_To_Rfal(pdTemp, param, 4, 5);
	ret = rfalTransceiveBlockingTxRx(param, 4*8+4, buffer, 1, &actLen, MIFARE_CLASSIC_FLAGS_WRITE, rfalConvMsTo1fc(50));
	if ((ret != ERR_INCOMPLETE_BYTE) || (actLen != 1)) {
		return ret = ERR_REQUEST;
	}
	ackTemp = buffer[0];
	crypto1_transcrypt_bits(cryptoState, &ackTemp, 0, 4);

	/* Data: */
	for (uint32_t i = 0; i < 16; i++) {
		pdTemp[i] = data[i];
	}
	crc = Mifare_Classic_CRC(pdTemp, 16);
	pdTemp[16] = crc & 0x00FF;	/* LSB. */
	pdTemp[17] = crc >> 8;		/* MSB. */
	Mifare_Classic_Parity(pdTemp, sizeof(pdTemp)/sizeof(parity_data_t));
	crypto1_transcrypt(cryptoState, pdTemp, sizeof(pdTemp)/sizeof(parity_data_t));
	Mifare_Classic_Crypto1_To_Rfal(pdTemp, param, 18, 21);
	ret = rfalTransceiveBlockingTxRx(param, 18*8+18, buffer, 1, &actLen, MIFARE_CLASSIC_FLAGS_WRITE, rfalConvMsTo1fc(50));
	if ((ret != ERR_INCOMPLETE_BYTE) || (actLen != 1)) {
		return ret = ERR_REQUEST;
	}

	return ret = ERR_NONE;

}

/***************************************************************************//**
	@brief		Process start.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Handle_Start(mifare_classic_handle_t *h) {

	/* Wait the ISO layer standby: */
	if (h->nfca->state == NFCA_HANDLE_STATE_STANDBY) {
		/* Search start: */
		h->nfca->control = NFCA_CONTROL_START;
		/* Wait the response: */
		h->state = MIFARE_CLASSIC_HANDLE_STATE_WAIT;
	}

}

/***************************************************************************//**
	@brief		Wait the response.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Handle_Wait(mifare_classic_handle_t *h) {

	/* Wait the ISO layer response: */
	if (h->nfca->state == NFCA_HANDLE_STATE_WAIT) {
		if (h->nfca->reg.flags.error) {
			h->state = MIFARE_CLASSIC_HANDLE_STATE_ERROR;
		}
		else {
			/* Check if can be some MIFARE Classic card: */
			if (h->nfca->card.cascadeLevels == 1) {
				h->state = MIFARE_CLASSIC_HANDLE_STATE_AUTH;
			}
			else {
				Nfc_Event_Callback(NFC_EVENT_NOT_FOUND, NFC_TYPE_MIFARE_CLASSIC, h);
				h->state = MIFARE_CLASSIC_HANDLE_STATE_STOP;
			}
		}
	}

}

/***************************************************************************//**
	@brief		Authentication procedure.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Handle_Auth(mifare_classic_handle_t *h) {

	uint8_t keyCmd;

	if (h->access.type == MIFARE_CLASSIC_KEY_A) {
		keyCmd = 0x60;
	}
	else {
		keyCmd = 0x61;
	}

	/* Start indication: */
	Nfc_Event_Callback(NFC_EVENT_START, NFC_TYPE_MIFARE_CLASSIC, h);
	if (Mifare_Classic_Auth(&h->nfca->card, &h->cryptoState, keyCmd, h->access.key, h->access.block) != ERR_NONE) {
		Nfc_Event_Callback(NFC_EVENT_NOT_FOUND, NFC_TYPE_MIFARE_CLASSIC, h);
		h->state = MIFARE_CLASSIC_HANDLE_STATE_STOP;
	}
	else {
		/* Identification report: */
		Nfc_Event_Callback(NFC_EVENT_IDENTIFICATION, NFC_TYPE_MIFARE_CLASSIC, h);
		/* Read request: */
		if ((h->reg.flags.rdRqst) && (h->rdBuffer != NULL)) {
			h->state = MIFARE_CLASSIC_HANDLE_STATE_READ;
		}
		/* Write request: */
		else if ((h->reg.flags.wrRqst) && (h->wrBuffer != NULL)) {
			h->state = MIFARE_CLASSIC_HANDLE_STATE_WRITE;
		}
		/* So, finished: */
		else {
			h->state = MIFARE_CLASSIC_HANDLE_STATE_STOP;
		}
	}

}


/***************************************************************************//**
	@brief		Block read.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Handle_Read(mifare_classic_handle_t *h) {

	h->reg.flags.rdRqst = 0;
	if (Mifare_Classic_Read(&h->cryptoState, h->access.block, h->rdBuffer) != ERR_NONE) {
		Nfc_Event_Callback(NFC_EVENT_READ_FAIL, NFC_TYPE_MIFARE_CLASSIC, h);
		h->state = MIFARE_CLASSIC_HANDLE_STATE_STOP;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_READ, NFC_TYPE_MIFARE_CLASSIC, h);
		/* Write request: */
		if ((h->reg.flags.wrRqst) && (h->wrBuffer != NULL)) {
			h->state = MIFARE_CLASSIC_HANDLE_STATE_WRITE;
		}
		/* So, finished: */
		else {
			h->state = MIFARE_CLASSIC_HANDLE_STATE_STOP;
		}
	}

}


/***************************************************************************//**
	@brief		Block write.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Handle_Write(mifare_classic_handle_t *h) {

	h->reg.flags.wrRqst = 0;
	if (Mifare_Classic_Write(&h->cryptoState, h->access.block, h->wrBuffer) != ERR_NONE) {
		Nfc_Event_Callback(NFC_EVENT_WRITE_FAIL, NFC_TYPE_MIFARE_CLASSIC, h);
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_WRITE, NFC_TYPE_MIFARE_CLASSIC, h);
	}

	/* So, finished: */
	h->state = MIFARE_CLASSIC_HANDLE_STATE_STOP;

}

/***************************************************************************//**
	@brief		Process stop.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Handle_Stop(mifare_classic_handle_t *h) {

	/* Releases the ISO layer: */
	h->nfca->control = NFCA_CONTROL_STOP;
	/* Releases the this layer: */
	h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	/* Resets the state machine: */
	h->state = MIFARE_CLASSIC_HANDLE_STATE_START;

}

/***************************************************************************//**
	@brief		Error handling.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Mifare_Classic_Handle_Error(mifare_classic_handle_t *h) {

	/* Error report: */
	Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_MIFARE_CLASSIC, h);
	/* Releases the ISO layer: */
	h->nfca->control = NFCA_CONTROL_STOP;
	/* Releases the this layer: */
	h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	/* Resets the state machine: */
	h->state = MIFARE_CLASSIC_HANDLE_STATE_START;

}

/***************************************************************************//**
	@brief		Driver initialization.

	Must be called in the application initialization.

	@param		param 	Driver handle structure.
	@param		iso		ISO handler.
	@return		None.
*******************************************************************************/

void Mifare_Classic_Init(void *param, void* iso) {

	mifare_classic_handle_t *h = (mifare_classic_handle_t *)param;

	h->reg.value = 0;

	h->state = MIFARE_CLASSIC_HANDLE_STATE_START;

	h->access.key = MIFARE_CLASSIC_DEFAULT_KEY;
	h->access.type = MIFARE_CLASSIC_DEFAULT_TYPE;
	h->access.block = MIFARE_CLASSIC_DEFAULT_BLOCK;

	h->protocolControl = NFC_PROTOCOL_CONTROL_READY;

	h->nfca = (nfca_handle_t *) iso;

}

/***************************************************************************//**
	@brief		Driver time base.

	Must be called in a 10ms routine.

	@param		param 	Driver handle structure.
	@return		None.
*******************************************************************************/

void Mifare_Classic_Tick(void *param) {

}

/***************************************************************************//**
	@brief		Driver handle.

	Must be called in the main loop.

	@param		param 	Driver handle structure.
	@return		None.
*******************************************************************************/

void Mifare_Classic_Handle(void *param) {

	mifare_classic_handle_t *h = (mifare_classic_handle_t *)param;

	/* State machine routines: */
	static void (*const fnc[])(mifare_classic_handle_t *h) = {
		Mifare_Classic_Handle_Start,		/**< MIFARE_CLASSIC_HANDLE_STATE_START. */
		Mifare_Classic_Handle_Wait,			/**< MIFARE_CLASSIC_HANDLE_STATE_WAIT. */
		Mifare_Classic_Handle_Auth,			/**< MIFARE_CLASSIC_HANDLE_STATE_AUTH. */
		Mifare_Classic_Handle_Read,			/**< MIFARE_CLASSIC_HANDLE_STATE_READ. */
		Mifare_Classic_Handle_Write,		/**< MIFARE_CLASSIC_HANDLE_STATE_WRITE. */
		Mifare_Classic_Handle_Stop,			/**< MIFARE_CLASSIC_HANDLE_STATE_STOP. */
		Mifare_Classic_Handle_Error,		/**< MIFARE_CLASSIC_HANDLE_STATE_ERROR. */
	};

	/* State machine: */
	if (h->protocolControl != NFC_PROTOCOL_CONTROL_FINISHED) {
		if (h->state < (sizeof(fnc)/sizeof(fnc[0]))) {
			fnc[h->state](h);
		}
	}

}

/***************************************************************************//**
	@brief		Useful routine to set the protocol control.
	@param		control	Control used by upon layers to control the chip usage.
	@param		param 	Driver handle structure.
	@return		None.
*******************************************************************************/

void Mifare_Classic_Control_Set(nfc_protocol_control_t control, void *param) {

	mifare_classic_handle_t *h = (mifare_classic_handle_t *)param;

	if (h->reg.flags.enable && (!h->reg.flags.block)) {
		/* Normal mode: */
		h->protocolControl = control;
	}
	else {
		/* Block mode: */
		h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	}


}

/***************************************************************************//**
	@brief		Useful routine to get the protocol control.
	@param		param 	Driver handle structure.
	@return		Control used by upon layers to control the chip usage.
*******************************************************************************/

nfc_protocol_control_t Mifare_Classic_Control_Get(void *param) {

	mifare_classic_handle_t *h = (mifare_classic_handle_t *)param;

	return h->protocolControl;

}

//----------------------------------------------------------------------------//
