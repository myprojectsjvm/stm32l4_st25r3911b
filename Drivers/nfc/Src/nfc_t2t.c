/***************************************************************************//**
	@file		nfc_t2t.c
	@brief		NFC Type 2 Tag driver.
********************************************************************************

	This driver interfaces with the NFC layer to provide the identification,
	read and write in some tag compliance with NFC Type 2 Tag.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		March-2021.
*******************************************************************************/

#include "nfc_t2t.h"

#include "rfal_t2t.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/**
 * Default access block.
 */
#define NFC_T2T_DEFAULT_BLOCK			0x05

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

static void Nfc_T2T_Handle_Start(nfc_t2t_handle_t *h);
static void Nfc_T2T_Handle_Wait(nfc_t2t_handle_t *h);
static void Nfc_T2T_Handle_Read(nfc_t2t_handle_t *h);
static void Nfc_T2T_Handle_Write(nfc_t2t_handle_t *h);
static void Nfc_T2T_Handle_Stop(nfc_t2t_handle_t *h);
static void Nfc_T2T_Handle_Error(nfc_t2t_handle_t *h);

/***************************************************************************//**
	@brief		Process start.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T2T_Handle_Start(nfc_t2t_handle_t *h) {

	/* Wait the ISO layer standby: */
	if (h->nfca->state == NFCA_HANDLE_STATE_STANDBY) {
		/* Search start: */
		h->nfca->control = NFCA_CONTROL_START;
		/* Wait the response: */
		h->state = NFC_T2T_HANDLE_STATE_WAIT;
	}

}

/***************************************************************************//**
	@brief		Wait the response.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T2T_Handle_Wait(nfc_t2t_handle_t *h) {

	/* Wait the ISO layer response: */
	if (h->nfca->state == NFCA_HANDLE_STATE_WAIT) {
		if (h->nfca->reg.flags.error) {
			h->state = NFC_T2T_HANDLE_STATE_ERROR;
		}
		else {
			/* Check if can be some NFC Type 2 Tag: */
			if ((h->nfca->card.cascadeLevels == 2) && ((h->nfca->card.sak[0] & 0x60)== 0x00)) {
				/* Identification report: */
				Nfc_Event_Callback(NFC_EVENT_IDENTIFICATION, NFC_TYPE_NFC_T2T, h);
				/* Read request: */
				if ((h->reg.flags.rdRqst) && (h->rdBuffer != NULL)) {
					h->state = NFC_T2T_HANDLE_STATE_READ;
				}
				/* Write request: */
				else if ((h->reg.flags.wrRqst) && (h->wrBuffer != NULL)) {
					h->state = NFC_T2T_HANDLE_STATE_WRITE;
				}
				/* So, finished: */
				else {
					h->state = NFC_T2T_HANDLE_STATE_STOP;
				}
			}
			else {
				Nfc_Event_Callback(NFC_EVENT_NOT_FOUND, NFC_TYPE_NFC_T2T, h);
				h->state = NFC_T2T_HANDLE_STATE_STOP;
			}
		}
	}

}

/***************************************************************************//**
	@brief		Block read.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T2T_Handle_Read(nfc_t2t_handle_t *h) {

	uint16_t rcvLen;

	h->reg.flags.rdRqst = 0;
	if ((rfalT2TPollerRead(h->block, h->rdBuffer, RFAL_T2T_READ_DATA_LEN, &rcvLen) != ERR_NONE) &&
		(rcvLen == RFAL_T2T_READ_DATA_LEN))
	{
		Nfc_Event_Callback(NFC_EVENT_READ_FAIL, NFC_TYPE_NFC_T2T, h);
		h->state = NFC_T2T_HANDLE_STATE_STOP;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_READ, NFC_TYPE_NFC_T2T, h);
		/* Write request: */
		if ((h->reg.flags.wrRqst) && (h->wrBuffer != NULL)) {
			h->state = NFC_T2T_HANDLE_STATE_WRITE;
		}
		/* So, finished: */
		else {
			h->state = NFC_T2T_HANDLE_STATE_STOP;
		}
	}

}


/***************************************************************************//**
	@brief		Block write.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T2T_Handle_Write(nfc_t2t_handle_t *h) {

	h->reg.flags.wrRqst = 0;
	if (rfalT2TPollerWrite(h->block, h->wrBuffer) != ERR_NONE) {
		Nfc_Event_Callback(NFC_EVENT_WRITE_FAIL, NFC_TYPE_NFC_T2T, h);
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_WRITE, NFC_TYPE_NFC_T2T, h);
	}

	/* So, finished: */
	h->state = NFC_T2T_HANDLE_STATE_STOP;

}

/***************************************************************************//**
	@brief		Process stop.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T2T_Handle_Stop(nfc_t2t_handle_t *h) {

	/* Releases the ISO layer: */
	h->nfca->control = NFCA_CONTROL_STOP;
	/* Releases the this layer: */
	h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	/* Resets the state machine: */
	h->state = NFC_T2T_HANDLE_STATE_START;

}

/***************************************************************************//**
	@brief		Error handling.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T2T_Handle_Error(nfc_t2t_handle_t *h) {

	/* Error report: */
	Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T2T, h);
	/* Releases the ISO layer: */
	h->nfca->control = NFCA_CONTROL_STOP;
	/* Releases the this layer: */
	h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	/* Resets the state machine: */
	h->state = NFC_T2T_HANDLE_STATE_START;

}

/***************************************************************************//**
	@brief		Driver initialization.

	Must be called in the application initialization.

	@param		param	Driver handle structure.
	@param		iso		ISO handler.
	@return		None.
*******************************************************************************/

void Nfc_T2T_Init(void *param, void *iso) {

	nfc_t2t_handle_t *h = (nfc_t2t_handle_t *)param;

	h->reg.value = 0;

	h->state = NFC_T2T_HANDLE_STATE_START;

	h->block = NFC_T2T_DEFAULT_BLOCK;

	h->protocolControl = NFC_PROTOCOL_CONTROL_READY;

	h->nfca = (nfca_handle_t *)iso;

}

/***************************************************************************//**
	@brief		Driver time base.

	Must be called in a 10ms routine.

	@param		param Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_T2T_Tick(void *param) {

}

/***************************************************************************//**
	@brief		Driver handle.

	Must be called in the main loop.

	@param		param Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_T2T_Handle(void *param) {

	nfc_t2t_handle_t *h = (nfc_t2t_handle_t *)param;

	/* State machine routines: */
	static void (*const fnc[])(nfc_t2t_handle_t *h) = {
		Nfc_T2T_Handle_Start,		/**< NFC_T2T_HANDLE_STATE_START. */
		Nfc_T2T_Handle_Wait,		/**< NFC_T2T_HANDLE_STATE_WAIT. */
		Nfc_T2T_Handle_Read,		/**< NFC_T2T_HANDLE_STATE_READ. */
		Nfc_T2T_Handle_Write,		/**< NFC_T2T_HANDLE_STATE_WRITE. */
		Nfc_T2T_Handle_Stop,		/**< NFC_T2T_HANDLE_STATE_STOP. */
		Nfc_T2T_Handle_Error,		/**< NFC_T2T_HANDLE_STATE_ERROR. */
	};

	/* State machine: */
	if (h->protocolControl != NFC_PROTOCOL_CONTROL_FINISHED) {
		if (h->state < (sizeof(fnc)/sizeof(fnc[0]))) {
			fnc[h->state](h);
		}
	}

}

/***************************************************************************//**
	@brief		Useful routine to set the protocol control.
	@param		control	Control used by upon layers to control the chip usage.
	@param		param 	Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_T2T_Control_Set(nfc_protocol_control_t control, void *param) {

	nfc_t2t_handle_t *h = (nfc_t2t_handle_t *)param;

	if (h->reg.flags.enable && (!h->reg.flags.block)) {
		/* Normal mode: */
		h->protocolControl = control;
	}
	else {
		/* Block mode: */
		h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	}


}

/***************************************************************************//**
	@brief		Useful routine to get the protocol control.
	@param		param 	Driver handle structure.
	@return		Control used by upon layers to control the chip usage.
*******************************************************************************/

nfc_protocol_control_t Nfc_T2T_Control_Get(void *param) {

	nfc_t2t_handle_t *h = (nfc_t2t_handle_t *)param;

	return h->protocolControl;

}

//----------------------------------------------------------------------------//
