/***************************************************************************//**
	@file		rfal_specifics.c
	@brief		NFC driver.
********************************************************************************

	Specific chip routines.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		December-2020.
*******************************************************************************/

#include "rfal_specifics.h"

#include "st25R3911.h"
#include "st25r3911_com.h"
#include "st25r3911_interrupt.h"

#include "rfal_chip.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

/***************************************************************************//**
	@brief		Complementary chip settings.
	@return		RFAL error code.
*******************************************************************************/

ReturnCode rfalChipSettings(void) {

	uint8_t temp[2] = {0};
	uint16_t mV;

	/* ST25R3911B settings: */
	st25r3911ModifyRegister(ST25R3911_REG_REGULATOR_CONTROL, ST25R3911_REG_REGULATOR_CONTROL_reg_s, 0x0);
	st25r3911AdjustRegulators(&mV);
	st25r3911ModifyRegister(ST25R3911_REG_ANT_CAL_CONTROL, ST25R3911_REG_ANT_CAL_CONTROL_trim_s, 0x0);
	st25r3911CalibrateAntenna(temp);

	return ERR_NONE;

}

/***************************************************************************//**
	@brief		Enables oscillator and regulator.
	@return		None.
*******************************************************************************/

void rfalChipEnable(void) {

	/* Ready mode: */
	st25r3911OscOn();

}

/***************************************************************************//**
	@brief		Disables oscillator and regulator
	@return		None.
*******************************************************************************/

void rfalChipDisable(void) {

	/* Off mode: */
	st25r3911ClrRegisterBits(ST25R3911_REG_OP_CONTROL, ST25R3911_REG_OP_CONTROL_en);

}

/***************************************************************************//**
	@brief		Wake-up settings.

	Wake-up interrupt by amplitude measurement.

	@return		RFAL error code.
*******************************************************************************/

ReturnCode rfalWakeupSettings(void) {

	uint8_t temp, addr, val;

	/*
	 * RF amplitude measurement. Can be used in the wake-up function.
	 */
	rfalChipMeasureAmplitude(&temp);

	/*
	 * Amplitude measurement reference register.
	 *
	 * am_ref = Measured value.
	 */
	addr = ST25R3911_REG_AMPLITUDE_MEASURE_REF;
	val  = temp;
	rfalChipWriteReg(addr, &val, 1);

	/*
	 * Amplitude measurement configuration register.
	 *
	 * am_d = 2 (difference to reference that triggers interrupt);
	 */
	addr = ST25R3911_REG_AMPLITUDE_MEASURE_CONF;
	val  = 0x28;
	rfalChipWriteReg(addr, &val, 1);

	/*
	 * Wake-up timer control register.
	 */
	addr = ST25R3911_REG_WUP_TIMER_CONTROL;
	val  = 0x04;
	rfalChipWriteReg(addr, &val, 1);

	/*
	 * Process start.
	 */
	rfalWakeupStart();

	return ERR_NONE;

}

/***************************************************************************//**
	@brief		Wake-up start.
	@return		RFAL error code.
*******************************************************************************/

ReturnCode rfalWakeupStart(void) {

	uint8_t addr, val;

	/*
	 * Operation control register.
	 */
	addr = ST25R3911_REG_OP_CONTROL;
	val  = 0x04;
	rfalChipWriteReg(addr, &val, 1);

	/*
	 * Mask error and wake-up interrupt register.
	 *
	 * Mask Wake-up IRQ due to amplitude measurement.
	 */
	addr = ST25R3911_REG_IRQ_MASK_ERROR_WUP;
	val  = 0xFB;
	rfalChipWriteReg(addr, &val, 1);

	return ERR_NONE;

}

/***************************************************************************//**
	@brief		Wake-up stop.
	@return		RFAL error code.
*******************************************************************************/

ReturnCode rfalWakeupStop(void) {

	uint8_t addr, val;

	/*
	 * Operation control register.
	 */
	addr = ST25R3911_REG_OP_CONTROL;
	val  = 0x80;
	rfalChipWriteReg(addr, &val, 1);

	/*
	 * Mask error and wake-up interrupt register.
	 *
	 * Mask Wake-up IRQ due to amplitude measurement.
	 */
	addr = ST25R3911_REG_IRQ_MASK_ERROR_WUP;
	val  = 0xFB;
	rfalChipWriteReg(addr, &val, 1);

	return ERR_NONE;

}

/***************************************************************************//**
	@brief		Wake-up detection.
	@return		TRUE in the wake-up detection and FALSE otherwise.
*******************************************************************************/

uint8_t rfalWakeupDetection(void) {

	uint8_t ret = 0x00;

	/* Wake-up interrupt due to phase measurement: */
	if (st25r3911GetInterrupt(1 << 18)) {
		ret = 0x01;
	}

	return ret;

}

//----------------------------------------------------------------------------//
