/***************************************************************************//**
	@file		nfca.c
	@brief		NFC-A commands.
********************************************************************************

	This driver is used to search for tags compliance with ISO14443-A.

	The following times was measured in each state of the main state machine
	with core clock in 48MHz and the SPI clock in 3MHz:

	Nfca_Handle_Handle_Field_On			: 00.44ms;
	Nfca_Handle_Handle_Init				: 00.87ms;
	Nfca_Handle_Handle_Guard_Time		: 00.00ms;
	Nfca_Handle_Handle_Request			: 01.55ms without card;
	Nfca_Handle_Handle_Request			: 01.70ms with card;
	Nfca_Handle_Handle_Anticollision	: 04.80ms;
	Nfca_Handle_Handle_Wait				: 00.00ms;
	Nfca_Handle_Handle_Deinit			: 00.10ms;

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		March-2021.
*******************************************************************************/

#include "nfca.h"

#include "rfal_nfca.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

static void Nfca_Handle_Standby(nfca_handle_t *h);
static void Nfca_Handle_Field_On(nfca_handle_t *h);
static void Nfca_Handle_Field_On(nfca_handle_t *h);
static void Nfca_Handle_Init(nfca_handle_t *h);
static void Nfca_Handle_Guard_Time(nfca_handle_t *h);
static void Nfca_Handle_Request(nfca_handle_t *h);
static void Nfca_Handle_Anticollision(nfca_handle_t *h);
static void Nfca_Handle_Wait(nfca_handle_t *h);
static void Nfca_Handle_Deinit(nfca_handle_t *h);
static void Nfca_Handle_Error(nfca_handle_t *h);

/***************************************************************************//**
	@brief		Wait to start.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfca_Handle_Standby(nfca_handle_t *h) {

	if (h->control == NFCA_CONTROL_START) {
		h->control = NFCA_CONTROL_NONE;
		h->state = NFCA_HANDLE_STATE_FIELD_ON;
	}

}

/***************************************************************************//**
	@brief		Turn on the field.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfca_Handle_Field_On(nfca_handle_t *h) {

	rfalSetGT(RFAL_TIMING_NONE);
	if (rfalFieldOnAndStartGT() != ERR_NONE) {
		h->reg.flags.error = 1;
		h->state = NFCA_HANDLE_STATE_WAIT;
	}
	else {
		h->state = NFCA_HANDLE_STATE_INIT;
	}

}

/***************************************************************************//**
	@brief		ISO14443A initialization.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfca_Handle_Init(nfca_handle_t *h) {

	if (iso14443AInitialize() != ERR_NONE) {
		h->reg.flags.error = 1;
		h->state = NFCA_HANDLE_STATE_WAIT;
	}
	else {
		h->state = NFCA_HANDLE_STATE_GUARD_TIME;
	}

}

/***************************************************************************//**
	@brief		Waits the minimum time after the field is turned on.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfca_Handle_Guard_Time(nfca_handle_t *h) {

	if (rfalIsGTExpired()) {
		h->state = NFCA_HANDLE_STATE_REQUEST;
	}

}

/***************************************************************************//**
	@brief		Request command.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfca_Handle_Request(nfca_handle_t *h) {

	ReturnCode err;
	uint16_t actlength;

	h->card.actlength = 0;

	err = rfalISO14443ATransceiveShortFrame(
		(rfal14443AShortFrameCmd)ISO14443A_CMD_REQA, (uint8_t*)&h->card.atqa,
		2*8, &actlength, RFAL_ISO14443A_INVENTORY_WAITING_TIME * 64
	);

	/* Select/anticollision not supported: */
	if ((ERR_NONE == err) && (0x00 == (h->card.atqa[0] & 0x1f))) {
		h->card.cascadeLevels = 0;		/* Indicates that there isn't some tag. */
		h->state = NFCA_HANDLE_STATE_WAIT;
	}
	/* No answer: */
	else if (ERR_TIMEOUT == err) {
		h->card.cascadeLevels = 0;		/* Indicates that there isn't some tag. */
		h->state = NFCA_HANDLE_STATE_WAIT;
	}
	/* At least one tag responded: */
	else {
		h->state = NFCA_HANDLE_STATE_ANTICOLLISION;
	}

}

/***************************************************************************//**
	@brief		Anticollision loop.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfca_Handle_Anticollision(nfca_handle_t *h) {

	if (rfalNfcaPollerSingleCollisionResolution(
		1, &h->card.collision, (rfalNfcaSelRes*)&h->card.sak[0],
		(uint8_t*)h->card.uid, &h->card.actlength ) != ERR_NONE)
	{
		/* Select/anticollision not supported: */
		h->card.cascadeLevels = 0;		/* Indicates that there isn't some tag. */
	}
	else {
		if(h->card.actlength == RFAL_NFCA_CASCADE_1_UID_LEN) {
			h->card.cascadeLevels = 1;	/* Indicates that there is some tag. */
		}
		else if(h->card.actlength == RFAL_NFCA_CASCADE_2_UID_LEN) {
			h->card.cascadeLevels = 2;	/* Indicates that there is some tag. */
		}
		else if(h->card.actlength == RFAL_NFCA_CASCADE_3_UID_LEN) {
			h->card.cascadeLevels = 3;	/* Indicates that there is some tag. */
		}
		else {
			h->card.cascadeLevels = 0;	/* Indicates that there isn't some tag. */
		}
	}
	h->state = NFCA_HANDLE_STATE_WAIT;

}

/***************************************************************************//**
	@brief		Wait to stop.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfca_Handle_Wait(nfca_handle_t *h) {

	if (h->control == NFCA_CONTROL_STOP) {
		h->control = NFCA_CONTROL_NONE;
		if (h->reg.flags.error) {
			h->reg.flags.error = 0;
			h->state = NFCA_HANDLE_STATE_ERROR;
		}
		else {
			h->state = NFCA_HANDLE_STATE_DEINIT;
		}
	}

}

/***************************************************************************//**
	@brief		Protocol deinitialization.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfca_Handle_Deinit(nfca_handle_t *h) {

	if (iso14443ADeinitialize(0) != ERR_NONE) {
		h->state = NFCA_HANDLE_STATE_ERROR;
	}
	else {
		h->state = NFCA_HANDLE_STATE_STANDBY;
	}

}

/***************************************************************************//**
	@brief		Error handling.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfca_Handle_Error(nfca_handle_t *h) {

	/* Try to turn off the field in case of error: */
	rfalFieldOff();

	/* Reset: */
	h->state = NFCA_HANDLE_STATE_STANDBY;

}

/***************************************************************************//**
	@brief		Driver initialization.

	Must be called in the application initialization.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfca_Init(nfca_handle_t *h) {

	h->reg.value = 0;
	h->state = NFCA_HANDLE_STATE_STANDBY;
	h->control = NFCA_CONTROL_NONE;

}

/***************************************************************************//**
	@brief		Driver handle.

	Must be called in the main loop.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfca_Handle(nfca_handle_t *h) {

	/* State machine routines: */
	static void (*const fnc[])(nfca_handle_t *h) = {
		Nfca_Handle_Standby,		/**< NFCA_HANDLE_STATE_STANDBY. */
		Nfca_Handle_Field_On,		/**< NFCA_HANDLE_STATE_FIELD_ON. */
		Nfca_Handle_Init,			/**< NFCA_HANDLE_STATE_INIT. */
		Nfca_Handle_Guard_Time,		/**< NFCA_HANDLE_STATE_GUARD_TIME. */
		Nfca_Handle_Request,		/**< NFCA_HANDLE_STATE_REQUEST. */
		Nfca_Handle_Anticollision,	/**< NFCA_HANDLE_STATE_ANTICOLLISION. */
		Nfca_Handle_Wait,			/**< NFCA_HANDLE_STATE_WAIT. */
		Nfca_Handle_Deinit,			/**< NFCA_HANDLE_STATE_DEINIT. */
		Nfca_Handle_Error,			/**< NFCA_HANDLE_STATE_ERROR. */
	};

	/* State machine: */
	if (h->state < (sizeof(fnc)/sizeof(fnc[0]))) {
		fnc[h->state](h);
	}

}

//----------------------------------------------------------------------------//
