/***************************************************************************//**
	@file		nfc.c
	@brief		NFC driver.
********************************************************************************

	This driver is the last layer that interfaces the NFC drivers with the main
	application.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		December-2020.
*******************************************************************************/

#include "nfc.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

/*
 * Main state machine routines.
 */
static void Nfc_Handle_Off(nfc_handle_t *h);
static void Nfc_Handle_Wait(nfc_handle_t *h);
static void Nfc_Handle_Start(nfc_handle_t *h);
static void Nfc_Handle_Run(nfc_handle_t *h);
static void Nfc_Handle_Error(nfc_handle_t *h);

/***************************************************************************//**
	@brief		NFC is off.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_Handle_Off(nfc_handle_t *h) {

	/* Turn on request: */
	if (h->reg.flags.on && h->protocol && h->n) {
		/* Ready mode: */
		rfalChipEnable();
		/* Wake-up mode request: */
		if (h->reg.flags.wakeupRqst) {
			/* Wake-up settings: */
			if (rfalWakeupSettings() != ERR_NONE) {
				/* Error handling: */
				h->state = NFC_HANDLE_STATE_ERROR;
			}
			else {
				/* Wake-up mode: */
				h->reg.flags.wakeupMode = 1;
				h->state = NFC_HANDLE_STATE_WAIT;
			}
		}
		/* Polling mode request: */
		else {
			h->counter = 0;
			h->reg.flags.wakeupMode = 0;
			h->state = NFC_HANDLE_STATE_WAIT;
		}
	}

}

/***************************************************************************//**
	@brief		Wait for some event (polling time or wake-up detection).
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_Handle_Wait(nfc_handle_t *h) {

	/* NFC is on: */
	if (h->reg.flags.on) {
		/* Wake-up mode: */
		if (h->reg.flags.wakeupMode) {
			if (rfalWakeupDetection()) {
				if (rfalWakeupStop() != ERR_NONE) {
					/* Error handling: */
					h->state = NFC_HANDLE_STATE_ERROR;
				}
				else {
					/* First protocol selection: */
					h->index = 0;
					h->state = NFC_HANDLE_STATE_START;
				}
			}
		}
		/* Polling mode: */
		else {
			if (!h->counter) {
				h->counter = h->period;
				/* First protocol selection: */
				h->index = 0;
				h->state = NFC_HANDLE_STATE_START;
			}
		}
	}
	/* Turn off request: */
	else {
		/* Off mode: */
		rfalChipDisable();
		h->state = NFC_HANDLE_STATE_OFF;
	}

}

/***************************************************************************//**
	@brief		Protocol start procedure.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_Handle_Start(nfc_handle_t *h) {

	/* Setting the ready state to wait the end of the process: */
	if (h->protocol[h->index].nfcProtocolControlSet != NULL) {
		h->protocol[h->index].nfcProtocolControlSet(
			NFC_PROTOCOL_CONTROL_READY,
			h->protocol[h->index].h
		);
		h->state = NFC_HANDLE_STATE_RUN;
	}
	else {
		/* Error handling: */
		h->state = NFC_HANDLE_STATE_ERROR;
	}

}

/***************************************************************************//**
	@brief		Protocol running state.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_Handle_Run(nfc_handle_t *h) {

	/* Waiting the end of the process: */
	if ((h->protocol[h->index].nfcProtocolControlGet != NULL) && (h->protocol[h->index].nfcProtocolHandle != NULL)) {
		if (h->protocol[h->index].nfcProtocolControlGet(h->protocol[h->index].h) ==
			NFC_PROTOCOL_CONTROL_FINISHED)
		{
			/* Next: */
			if (++h->index < h->n) {
				h->state = NFC_HANDLE_STATE_START;
			}
			/* All protocols was processed: */
			else {
				/* Wake-up mode: */
				if (h->reg.flags.wakeupMode) {
					if (rfalWakeupStart() != ERR_NONE) {
						/* Error handling: */
						h->state = NFC_HANDLE_STATE_ERROR;
					}
					else {
						h->state = NFC_HANDLE_STATE_WAIT;
					}
				}
				/* Polling mode: */
				else {
					h->state = NFC_HANDLE_STATE_WAIT;
				}
			}
		}
		else {
			h->protocol[h->index].nfcProtocolHandle(h->protocol[h->index].h);
		}
	}
	else {
		/* Error handling: */
		h->state = NFC_HANDLE_STATE_ERROR;
	}

}

/***************************************************************************//**
	@brief		Error state.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_Handle_Error(nfc_handle_t *h) {

	Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NONE, h);

	/* Off mode: */
	rfalChipDisable();

	/* Turn-off to prevent multiple errors: */
	h->reg.flags.on = 0;
	h->state = NFC_HANDLE_STATE_OFF;

}

/***************************************************************************//**
	@brief		Driver initialization.

	Must be called in the application initialization.

	@param		h 			Driver handle structure.
	@param		protocol	Protocol routines.
	@param		n			Number of protocols that are supported.
	@return		Driver error code.
*******************************************************************************/

nfc_error_t Nfc_Init(nfc_handle_t *h, const nfc_protocol_t *protocols, uint32_t n) {

	nfc_error_t error = NFC_ERROR_OK;
	void *iso;

	h->reg.value = 0;
	h->state = NFC_HANDLE_STATE_OFF;

	h->protocol = protocols;
	h->n = n;

	/* ISO layers initialization: */
	Nfca_Init(&h->nfca);
	Nfcv_Init(&h->nfcv);

	/* Protocols initialization: */
	for (uint32_t i = 0; i < h->n; i++) {
		if (h->protocol[i].nfcProtocolInit != NULL) {
			if (h->protocol[i].iso == NFC_ISO14443_A) {
				iso = (void *)&h->nfca;
			}
			else if (h->protocol[i].iso == NFC_ISO15693) {
				iso = (void *)&h->nfcv;
			}
			else {
				iso = NULL;
			}
			h->protocol[i].nfcProtocolInit(h->protocol[i].h, iso);
		}
	}

	/* RFAL initialization: */
	rfalAnalogConfigInitialize();
	if (rfalInitialize() != ERR_NONE) {
		error = NFC_ERROR_FAIL;
	}
	/* Specific chip initializations: */
	else {
		if (rfalChipSettings() != ERR_NONE) {
			error = NFC_ERROR_FAIL;
		}
	}
	/* Off mode: */
	rfalChipDisable();

	return error;

}

/***************************************************************************//**
	@brief		Driver time base.

	Must be called in a 10ms routine.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_Tick(nfc_handle_t *h) {

	/* Driver counter: */
	if (h->counter) h->counter--;

	/* Protocols time base: */
	for (uint32_t i = 0; i < h->n; i++) {
		if (h->protocol[i].nfcProtocolTick != NULL) {
			h->protocol[i].nfcProtocolTick(h->protocol[i].h);
		}
	}

}

/***************************************************************************//**
	@brief		Driver handle.

	Must be called in the main loop.

	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_Handle(nfc_handle_t *h) {

	/* State machine routines: */
	static void (*const fnc[])(nfc_handle_t *h) = {
		Nfc_Handle_Off,		/**< NFC_HANDLE_STATE_OFF. */
		Nfc_Handle_Wait,	/**< NFC_HANDLE_STATE_WAIT. */
		Nfc_Handle_Start,	/**< NFC_HANDLE_STATE_START. */
		Nfc_Handle_Run,		/**< NFC_HANDLE_STATE_RUN. */
		Nfc_Handle_Error,	/**< NFC_HANDLE_STATE_ERROR. */
	};

	/* NFC driver handler: */
	rfalWorker();

	/* ISO layers processing: */
	Nfca_Handle(&h->nfca);
	Nfcv_Handle(&h->nfcv);

	/* State machine: */
	if (h->state < (sizeof(fnc)/sizeof(fnc[0]))) {
		fnc[h->state](h);
	}

}

/***************************************************************************//**
	@brief		NFC event callback.
	@param		event	NFC event.
	@param		type	NFC type.
	@param		h		Driver handle structure.
	@return		None.
*******************************************************************************/

__weak void Nfc_Event_Callback(nfc_event_t event, nfc_type_t type, void *h) {

	UNUSED(event);
	UNUSED(type);
	UNUSED(h);

	/**
	 * @note	This function should not be modified, when the callback is
	 * 			needed, this function should be implemented in the user file.
	 */

}

//----------------------------------------------------------------------------//
