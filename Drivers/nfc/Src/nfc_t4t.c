/***************************************************************************//**
	@file		nfc_t4t.c
	@brief		NFC Type 4 Tag driver.
********************************************************************************

	This driver interfaces with the NFC layer to provide the identification,
	read and write in some tag compliance with NFC Type 4 Tag.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		March-2021.
*******************************************************************************/

#include "nfc_t4t.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/**
 * NDEF Tag Application Select.
 */
const uint8_t NfcT4TCmdNDEFTagAppSelect[] = {
	/* Prologue: */
	0x00, 0x00, 0x00,
	/* NDEF Tag Application Select command: */
	0x00, 0xA4, 0x04, 0x00, 0x07, 0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x01, 0x00
};

/**
 * Capability Container Select.
 */
const uint8_t NfcT4TCmdCCSelect[] = {
	/* Prologue: */
	0x00, 0x00, 0x00,
	/* Capability Container Select command: */
	0x00, 0xA4, 0x00, 0x0C, 0x02, 0xE1, 0x03
};

/**
 * ReadBinary.
 */
const uint8_t NfcT4TCmdReadBinary[] = {
	/* Prologue: */
	0x00, 0x00, 0x00,
	/* ReadBinary command: */
	0x00, 0xB0, 0x00, 0x00, 0x00,
};

/**
 * NDEF Select.
 */
const uint8_t NfcT4TCmdNDEFSelect[] = {
	/* Prologue: */
	0x00, 0x00, 0x00,
	/* NDEF Select command: */
	0x00, 0xA4, 0x00, 0x0C, 0x02, 0x00, 0x00
};

/**
 * UpdateBinary.
 */
const uint8_t NfcT4TCmdUpdateBinary[] = {
	/* Prologue: */
	0x00, 0x00, 0x00,
	/* UpdateBinary command: */
	0x00, 0xD6, 0x00, 0x00, 0x00,
};

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

rfalIsoDepApduTxRxParam Nfc_T4T_Apdu_Fill(nfc_t4t_handle_t *h, const uint8_t *cmd, uint8_t size);

static void Nfc_T4T_Handle_Start(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_Wait(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_Apdu_Status(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_App_Select(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_CC_Select(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_CC_Length(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_CC_File(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_NDEF_Select(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_NDEF_Length_Read(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_NDEF_File_Read(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_NDEF_Length_Erase(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_NDEF_File_Write(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_NDEF_Length_Write(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_Stop(nfc_t4t_handle_t *h);
static void Nfc_T4T_Handle_Error(nfc_t4t_handle_t *h);

/***************************************************************************//**
	@brief		APDU (Application Protocol Data Unit) parameter fill.
	@param		h		Driver handle structure.
	@param		cmd		APDU command.
	@param		size	APDU size.
	@return		None.
*******************************************************************************/

rfalIsoDepApduTxRxParam Nfc_T4T_Apdu_Fill(nfc_t4t_handle_t *h, const uint8_t *cmd, uint8_t size) {

	rfalIsoDepApduTxRxParam ApduParam;

	/* Verification just to prevent some unexpected error: */
	if (size <= NFC_T4T_CMD_MAX) {
		/* Fill: */
		for (uint32_t i = 0; i < size; i++) {
			h->cmd[i] = cmd[i];
		}
		/**
		 * @attention The "txBuf" pointer is modified by RFAL
		 * layers, so, can't pointer to a constant vector.
		 */
		ApduParam.txBuf		= (rfalIsoDepApduBufFormat *)h->cmd;
		ApduParam.txBufLen	= size-3;
		ApduParam.rxBuf		= &h->rxBuf;
		ApduParam.rxLen 	= &h->rxLen;
		ApduParam.tmpBuf	= &h->tmpBuf;
		ApduParam.FWT		= 0x20000;
		ApduParam.dFWT		= 16;
		ApduParam.FSx		= 255;
		ApduParam.ourFSx	= 255;
		ApduParam.DID		= 0;
	}
	else {
		/* Error report: */
		Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T4T, h);
	}

	return ApduParam;

}

/***************************************************************************//**
	@brief		Process start.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_Start(nfc_t4t_handle_t *h) {

	/* Wait the ISO layer standby: */
	if (h->nfca->state == NFCA_HANDLE_STATE_STANDBY) {
		/* Search start: */
		h->nfca->control = NFCA_CONTROL_START;
		/* Wait the response: */
		h->state = NFC_T4T_HANDLE_STATE_WAIT;
	}

}

/***************************************************************************//**
	@brief		Wait the response.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_Wait(nfc_t4t_handle_t *h) {

	/* Wait the ISO layer response: */
	if (h->nfca->state == NFCA_HANDLE_STATE_WAIT) {
		if (h->nfca->reg.flags.error) {
			h->state = NFC_T4T_HANDLE_STATE_ERROR;
		}
		else {
			/* Check if can be some NFC Type 4 Tag: */
			if ((h->nfca->card.cascadeLevels == 2) && (h->nfca->card.sak[0] & 0x20)) {
				/* ISO-DEP initialization: */
				rfalIsoDepInitialize();
				if (rfalIsoDepPollAHandleActivation(RFAL_ISODEP_FSXI_256, 0, RFAL_BR_106, &h->isoDepDevice) == ERR_NONE) {
					/* Identification report: */
					Nfc_Event_Callback(NFC_EVENT_IDENTIFICATION, NFC_TYPE_NFC_T4T, h);
					/* Read request: */
					if (h->reg.flags.rdRqst && (h->rdBuffer != NULL)) {
						h->state = NFC_T4T_HANDLE_STATE_APP_SELECT;
					}
					/* Write request: */
					else if (h->reg.flags.wrRqst && (h->wrBuffer != NULL)) {
						h->state = NFC_T4T_HANDLE_STATE_APP_SELECT;
					}
					/* So, finished: */
					else {
						h->state = NFC_T4T_HANDLE_STATE_STOP;
					}
				}
				else {
					Nfc_Event_Callback(NFC_EVENT_NOT_FOUND, NFC_TYPE_NFC_T4T, h);
					h->state = NFC_T4T_HANDLE_STATE_STOP;
				}
			}
			else {
				Nfc_Event_Callback(NFC_EVENT_NOT_FOUND, NFC_TYPE_NFC_T4T, h);
				h->state = NFC_T4T_HANDLE_STATE_STOP;
			}
		}
	}

}

/***************************************************************************//**
	@brief		APDU (Application Protocol Data Unit) status.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_Apdu_Status(nfc_t4t_handle_t *h) {

	ReturnCode err;

	err = rfalIsoDepGetApduTransceiveStatus();

	if (err != ERR_BUSY) {
		/* Command completed verification: */
		if ((err == ERR_NONE) 					&&
			(h->rxLen >= 2) 					&&
			(h->rxBuf.apdu[h->rxLen-2] == 0x90) &&
			(h->rxBuf.apdu[h->rxLen-1] == 0x00))
		{
			h->state = h->next;
		}
		else {
			Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}
	}

}

/***************************************************************************//**
	@brief		NDEF Tag Application Select procedure.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_App_Select(nfc_t4t_handle_t *h) {

	rfalIsoDepApduTxRxParam ApduParam;

	/* Fill: */
	ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdNDEFTagAppSelect, sizeof(NfcT4TCmdNDEFTagAppSelect));
	/* Start: */
	if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
		h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
		h->next = NFC_T4T_HANDLE_STATE_CC_SELECT;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T4T, h);
		h->state = NFC_T4T_HANDLE_STATE_STOP;
	}

}

/***************************************************************************//**
	@brief		Capability Container Select procedure.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_CC_Select(nfc_t4t_handle_t *h) {

	rfalIsoDepApduTxRxParam ApduParam;

	/* Fill: */
	ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdCCSelect, sizeof(NfcT4TCmdCCSelect));
	/* Start: */
	if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
		h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
		h->next = NFC_T4T_HANDLE_STATE_CC_LENGTH;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T4T, h);
		h->state = NFC_T4T_HANDLE_STATE_STOP;
	}

}

/***************************************************************************//**
	@brief		CC file length read.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_CC_Length(nfc_t4t_handle_t *h) {

	rfalIsoDepApduTxRxParam ApduParam;

	/* Fill: */
	ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdReadBinary, sizeof(NfcT4TCmdReadBinary));
	h->cmd[5] = 0x00;				/* Offset (MSB). */
	h->cmd[6] = 0x00;				/* Offset (LSB). */
	h->cmd[7] = 0x02;				/* Size. */
	/* Start: */
	if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
		h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
		h->next = NFC_T4T_HANDLE_STATE_CC_FILE;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T4T, h);
		h->state = NFC_T4T_HANDLE_STATE_STOP;
	}

}

/***************************************************************************//**
	@brief		CC file content read.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_CC_File(nfc_t4t_handle_t *h) {

	rfalIsoDepApduTxRxParam ApduParam;

	/* Fill: */
	ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdReadBinary, sizeof(NfcT4TCmdReadBinary));
	h->cmd[5] = 0x00;				/* Offset (MSB). */
	h->cmd[6] = 0x02;				/* Offset (LSB). */
	h->cmd[7] = h->rxBuf.apdu[1]-2;	/* Size. */
	/* Start: */
	if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
		h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
		h->next = NFC_T4T_HANDLE_STATE_NDEF_SELECT;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T4T, h);
		h->state = NFC_T4T_HANDLE_STATE_STOP;
	}

}

/***************************************************************************//**
	@brief		NDEF Select procedure.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_NDEF_Select(nfc_t4t_handle_t *h) {

	rfalIsoDepApduTxRxParam ApduParam;

	/* Read request: */
	if (h->reg.flags.rdRqst) {
		h->ndefMax = h->rxBuf.apdu[2];
		/* Protect card not supported: */
		if (h->rxBuf.apdu[11]) {
			Nfc_Event_Callback(NFC_EVENT_READ_FAIL, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}
	}
	/* Write request: */
	else /* if (h->reg.flags.wrRqst) */ {
		h->ndefMax = h->rxBuf.apdu[4];
		/* Protect card not supported: */
		if (h->rxBuf.apdu[12]) {
			Nfc_Event_Callback(NFC_EVENT_WRITE_FAIL, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}
	}

	/* NDEF selection: */
	if (h->state == NFC_T4T_HANDLE_STATE_NDEF_SELECT) {
		/* Fill: */
		ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdNDEFSelect, sizeof(NfcT4TCmdNDEFSelect));
		h->cmd[8] = h->rxBuf.apdu[7];	/* Parameter from CC file (MSB of the file identifier). */
		h->cmd[9] = h->rxBuf.apdu[8];	/* Parameter from CC file (LSB of the file identifier). */
		/* Start: */
		if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
			h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
			/* Read request: */
			if (h->reg.flags.rdRqst) {
				h->next = NFC_T4T_HANDLE_STATE_NDEF_LENGTH_READ;
			}
			/* Write request: */
			else /* if (h->reg.flags.wrRqst) */ {
				h->next = NFC_T4T_HANDLE_STATE_NDEF_LENGTH_ERASE;
			}
		}
		else {
			Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}

	}

}

/***************************************************************************//**
	@brief		NDEF length read.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_NDEF_Length_Read(nfc_t4t_handle_t *h) {

	rfalIsoDepApduTxRxParam ApduParam;

	/* Fill: */
	ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdReadBinary, sizeof(NfcT4TCmdReadBinary));
	h->cmd[5] = 0x00;				/* Offset (MSB). */
	h->cmd[6] = 0x00;				/* Offset (LSB). */
	h->cmd[7] = 0x02;				/* Size. */
	/* Start: */
	if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
		h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
		h->next = NFC_T4T_HANDLE_STATE_NDEF_FILE_READ;
		h->ndefIdx = 0;
		h->ndefSize = 0;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_READ_FAIL, NFC_TYPE_NFC_T4T, h);
		h->state = NFC_T4T_HANDLE_STATE_STOP;
	}

}

/***************************************************************************//**
	@brief		NDEF content read.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_NDEF_File_Read(nfc_t4t_handle_t *h) {

	uint8_t read = 0, size;
	uint16_t offset, remaining;
	rfalIsoDepApduTxRxParam ApduParam;

	/* Size reception (first interaction): */
	if (h->ndefSize == 0) {
		h->ndefSize = (h->rxBuf.apdu[0] << 8) | h->rxBuf.apdu[1];
		if (h->ndefSize == 0) {
			h->reg.flags.rdRqst = 0;
			Nfc_Event_Callback(NFC_EVENT_READ, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}
		else if (h->ndefSize <= h->rdSize) {
			read = 1;
		}
		else {
			Nfc_Event_Callback(NFC_EVENT_READ_FAIL, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}
	}
	/* Content read: */
	else {
		/* Copy: */
		for (uint32_t i = 0; i < (h->rxLen - 2); i++) {
			h->rdBuffer[h->ndefIdx++] = h->rxBuf.apdu[i];
		}
		/* All bytes received: */
		if (h->ndefIdx == h->ndefSize) {
			h->reg.flags.rdRqst = 0;
			Nfc_Event_Callback(NFC_EVENT_READ, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}
		/* Request more data: */
		else {
			read = 1;
		}
	}

	/* Read interaction: */
	if (read) {
		/* Data to be read: */
		offset = 2 + h->ndefIdx;
		remaining = h->ndefSize - h->ndefIdx;
		if (remaining <= h->ndefMax) {
			size = remaining;
		}
		else {
			size = h->ndefMax;
		}
		/* Fill: */
		ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdReadBinary, sizeof(NfcT4TCmdReadBinary));
		h->cmd[5] = (uint8_t)(offset >> 8);	/* Offset (MSB). */
		h->cmd[6] = (uint8_t)(offset);		/* Offset (LSB). */
		h->cmd[7] = size;					/* Size. */
		/* Start: */
		if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
			h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
			h->next = NFC_T4T_HANDLE_STATE_NDEF_FILE_READ;
		}
		else {
			Nfc_Event_Callback(NFC_EVENT_NOT_FOUND, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}
	}

}

/***************************************************************************//**
	@brief		NDEF length erase.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_NDEF_Length_Erase(nfc_t4t_handle_t *h) {

	rfalIsoDepApduTxRxParam ApduParam;

	/* Fill: */
	ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdUpdateBinary, sizeof(NfcT4TCmdUpdateBinary));
	h->cmd[5] = 0x00;				/* Offset (MSB). */
	h->cmd[6] = 0x00;				/* Offset (LSB). */
	h->cmd[7] = 0x02;				/* Size. */
	/* Data: */
	h->cmd[8] = 0x00;
	h->cmd[9] = 0x00;
	ApduParam.txBufLen += 2;
	/* Start: */
	if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
		h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
		h->next = NFC_T4T_HANDLE_STATE_NDEF_FILE_WRITE;
		h->ndefIdx = 0;
	}
	else {
		Nfc_Event_Callback(NFC_EVENT_WRITE_FAIL, NFC_TYPE_NFC_T4T, h);
		h->state = NFC_T4T_HANDLE_STATE_STOP;
	}

}


/***************************************************************************//**
	@brief		NDEF content write.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_NDEF_File_Write(nfc_t4t_handle_t *h) {

	uint8_t size;
	uint16_t offset, remaining;
	rfalIsoDepApduTxRxParam ApduParam;

	/* Data write: */
	if (h->ndefIdx < h->wrSize) {
		/* Data to be read: */
		offset = 2 + h->ndefIdx;
		remaining = h->wrSize - h->ndefIdx;
		if (remaining <= h->ndefMax) {
			size = remaining;
		}
		else {
			size = h->ndefMax;
		}
		/* Fill: */
		ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdUpdateBinary, sizeof(NfcT4TCmdUpdateBinary));
		h->cmd[5] = (uint8_t)(offset >> 8);	/* Offset (MSB). */
		h->cmd[6] = (uint8_t)(offset);		/* Offset (LSB). */
		h->cmd[7] = size;					/* Size. */
		/* Data: */
		for (uint32_t i = 0; i < size; i++) {
			h->cmd[8+i] = h->wrBuffer[h->ndefIdx++];
		}
		ApduParam.txBufLen += size;
		/* Start: */
		if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
			h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
			h->next = NFC_T4T_HANDLE_STATE_NDEF_FILE_WRITE;
		}
		else {
			Nfc_Event_Callback(NFC_EVENT_WRITE_FAIL, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}
	}
	/* Length write: */
	else {
		/* Fill: */
		ApduParam = Nfc_T4T_Apdu_Fill(h, NfcT4TCmdUpdateBinary, sizeof(NfcT4TCmdUpdateBinary));
		h->cmd[5] = 0x00;				/* Offset (MSB). */
		h->cmd[6] = 0x00;				/* Offset (LSB). */
		h->cmd[7] = 0x02;				/* Size. */
		/* Data: */
		h->cmd[8] = (uint8_t)(h->ndefIdx >> 8);
		h->cmd[9] = (uint8_t)(h->ndefIdx);
		ApduParam.txBufLen += 2;
		/* Start: */
		if (rfalIsoDepStartApduTransceive(ApduParam) == ERR_NONE) {
			h->state = NFC_T4T_HANDLE_STATE_APDU_STATUS;
			h->next = NFC_T4T_HANDLE_STATE_NDEF_LENGTH_WRITE;
		}
		else {
			Nfc_Event_Callback(NFC_EVENT_WRITE_FAIL, NFC_TYPE_NFC_T4T, h);
			h->state = NFC_T4T_HANDLE_STATE_STOP;
		}
	}

}

/***************************************************************************//**
	@brief		NDEF length write.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_NDEF_Length_Write(nfc_t4t_handle_t *h) {

	h->reg.flags.wrRqst = 0;
	Nfc_Event_Callback(NFC_EVENT_WRITE, NFC_TYPE_NFC_T4T, h);
	h->state = NFC_T4T_HANDLE_STATE_STOP;

}

/***************************************************************************//**
	@brief		Process stop.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_Stop(nfc_t4t_handle_t *h) {

	/* Releases the ISO layer: */
	h->nfca->control = NFCA_CONTROL_STOP;
	/* Releases the this layer: */
	h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	/* Resets the state machine: */
	h->state = NFC_T4T_HANDLE_STATE_START;

}

/***************************************************************************//**
	@brief		Error handling.
	@param		h Driver handle structure.
	@return		None.
*******************************************************************************/

static void Nfc_T4T_Handle_Error(nfc_t4t_handle_t *h) {

	/* Error report: */
	Nfc_Event_Callback(NFC_EVENT_ERROR, NFC_TYPE_NFC_T4T, h);
	/* Releases the ISO layer: */
	h->nfca->control = NFCA_CONTROL_STOP;
	/* Releases the this layer: */
	h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	/* Resets the state machine: */
	h->state = NFC_T4T_HANDLE_STATE_START;

}

/***************************************************************************//**
	@brief		Driver initialization.

	Must be called in the application initialization.

	@param		param	Driver handle structure.
	@param		iso		ISO handler.
	@return		None.
*******************************************************************************/

void Nfc_T4T_Init(void *param, void *iso) {

	nfc_t4t_handle_t *h = (nfc_t4t_handle_t *)param;

	h->reg.value = 0;

	h->state = NFC_T4T_HANDLE_STATE_START;

	h->protocolControl = NFC_PROTOCOL_CONTROL_READY;

	h->nfca = (nfca_handle_t *)iso;

}

/***************************************************************************//**
	@brief		Driver time base.

	Must be called in a 10ms routine.

	@param		param Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_T4T_Tick(void *param) {

}

/***************************************************************************//**
	@brief		Driver handle.

	Must be called in the main loop.

	@param		param Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_T4T_Handle(void *param) {

	nfc_t4t_handle_t *h = (nfc_t4t_handle_t *)param;

	/* State machine routines: */
	static void (*const fnc[])(nfc_t4t_handle_t *h) = {
		Nfc_T4T_Handle_Start,				/**< NFC_T4T_HANDLE_STATE_START. */
		Nfc_T4T_Handle_Wait,				/**< NFC_T4T_HANDLE_STATE_WAIT. */
		Nfc_T4T_Handle_Apdu_Status,			/**< NFC_T4T_HANDLE_STATE_APDU_STATUS. */
		Nfc_T4T_Handle_App_Select,			/**< NFC_T4T_HANDLE_STATE_APP_SELECT. */
		Nfc_T4T_Handle_CC_Select,			/**< NFC_T4T_HANDLE_STATE_CC_SELECT. */
		Nfc_T4T_Handle_CC_Length,			/**< NFC_T4T_HANDLE_STATE_CC_LENGTH. */
		Nfc_T4T_Handle_CC_File,				/**< NFC_T4T_HANDLE_STATE_CC_FILE. */
		Nfc_T4T_Handle_NDEF_Select,			/**< NFC_T4T_HANDLE_STATE_NDEF_SELECT. */
		Nfc_T4T_Handle_NDEF_Length_Read,	/**< NFC_T4T_HANDLE_STATE_NDEF_LENGTH_READ. */
		Nfc_T4T_Handle_NDEF_File_Read,		/**< NFC_T4T_HANDLE_STATE_NDEF_FILE_READ. */
		Nfc_T4T_Handle_NDEF_Length_Erase,	/**< NFC_T4T_HANDLE_STATE_NDEF_LENGTH_ERASE. */
		Nfc_T4T_Handle_NDEF_File_Write,		/**< NFC_T4T_HANDLE_STATE_NDEF_FILE_WRITE. */
		Nfc_T4T_Handle_NDEF_Length_Write,	/**< NFC_T4T_HANDLE_STATE_NDEF_LENGTH_WRITE. */
		Nfc_T4T_Handle_Stop,				/**< NFC_T4T_HANDLE_STATE_STOP. */
		Nfc_T4T_Handle_Error,				/**< NFC_T4T_HANDLE_STATE_ERROR. */
	};

	/* State machine: */
	if (h->protocolControl != NFC_PROTOCOL_CONTROL_FINISHED) {
		if (h->state < (sizeof(fnc)/sizeof(fnc[0]))) {
			fnc[h->state](h);
		}
	}

}

/***************************************************************************//**
	@brief		Useful routine to set the protocol control.
	@param		control	Control used by upon layers to control the chip usage.
	@param		param 	Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_T4T_Control_Set(nfc_protocol_control_t control, void *param) {

	nfc_t4t_handle_t *h = (nfc_t4t_handle_t *)param;

	if (h->reg.flags.enable && (!h->reg.flags.block)) {
		/* Normal mode: */
		h->protocolControl = control;
	}
	else {
		/* Block mode: */
		h->protocolControl = NFC_PROTOCOL_CONTROL_FINISHED;
	}


}

/***************************************************************************//**
	@brief		Useful routine to get the protocol control.
	@param		param 	Driver handle structure.
	@return		Control used by upon layers to control the chip usage.
*******************************************************************************/

nfc_protocol_control_t Nfc_T4T_Control_Get(void *param) {

	nfc_t4t_handle_t *h = (nfc_t4t_handle_t *)param;

	return h->protocolControl;

}

//----------------------------------------------------------------------------//
