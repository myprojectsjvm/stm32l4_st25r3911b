/***************************************************************************//**
	@file		nfc_common.h
	@brief		NFC driver.
********************************************************************************

	Common types used by all protocols.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		December-2020.
*******************************************************************************/

#ifndef NFC_COMMON_H_
#define NFC_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "mcu_includes.h"

#include "rfal_analogConfig.h"
#include "rfal_chip.h"
#include "rfal_specifics.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Protocol control.
 */
typedef enum {

	NFC_PROTOCOL_CONTROL_READY = 0x00,			/**< Ready to start. */
	NFC_PROTOCOL_CONTROL_BUSY,					/**< Hunting some tag. */
	NFC_PROTOCOL_CONTROL_FINISHED,				/**< Process finished. */

} nfc_protocol_control_t;

/**
 * NFC events.
 */
typedef enum {

	NFC_EVENT_START = 0x00,						/**< Procedure start. */
	NFC_EVENT_IDENTIFICATION,					/**< The tag was identified. */
	NFC_EVENT_READ,								/**< The tag was read. */
	NFC_EVENT_READ_FAIL,						/**< Read failure. */
	NFC_EVENT_WRITE,							/**< The tag was write. */
	NFC_EVENT_WRITE_FAIL,						/**< Write failure. */
	NFC_EVENT_NOT_FOUND,						/**< The tag was not found. */
	NFC_EVENT_ERROR,							/**< Unexpected error. */

} nfc_event_t;

/**
 * NFC types.
 */
typedef enum {

	NFC_TYPE_NONE = 0x00,						/**< Used by the upon layer. */
	NFC_TYPE_NFCA,								/**< ISO14443-A layer. */
	NFC_TYPE_MIFARE_CLASSIC,					/**< NXP MIFARE Classic®. */
	NFC_TYPE_NFC_T2T,							/**< NFC Type 2 Tag. */
	NFC_TYPE_NFC_T4T,							/**< NFC Type 4 Tag. */
	NFC_TYPE_NFC_T5T,							/**< NFC Type 4 Tag. */
	NFC_TYPE_MAX,								/**< Limitation. */

} nfc_type_t;

/**
 *
 */
typedef enum {

	NFC_ISO14443_A = 0x00,						/**< ISO14443-A layer. */
	NFC_ISO15693,								/**< ISO15693 layer. */

} nfc_iso_t;

/**
 * Protocol structure.
 */
typedef struct {

	/**
	 * Type.
	 */
	nfc_type_t type;

	/**
	 * Type.
	 */
	nfc_iso_t iso;

	/**
	 * Handle variable.
	 */
	void *h;

	/**
	 * Protocol initialization.
	 */
	void (*nfcProtocolInit)(void *param, void* iso);

	/**
	 * Protocol time base.
	 */
	void (*nfcProtocolTick)(void *param);

	/**
	 * Protocol handle
	 */
	void (*nfcProtocolHandle)(void *param);

	/**
	 * Set state.
	 */
	void (*nfcProtocolControlSet)(nfc_protocol_control_t state, void *param);

	/**
	 * Get state.
	 */
	nfc_protocol_control_t (*nfcProtocolControlGet)(void *param);

} nfc_protocol_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

void Nfc_Event_Callback(nfc_event_t event, nfc_type_t type, void *h);

//----------------------------------------------------------------------------//

#ifdef __cplusplus
}
#endif

#endif /* NFC_COMMON_H_ */

//----------------------------------------------------------------------------//
