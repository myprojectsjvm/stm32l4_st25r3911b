/***************************************************************************//**
	@file		nfc_t5t.h
	@brief		NFC Type 5 Tag driver.
********************************************************************************

	This driver interfaces with the NFC layer to provide the identification,
	read and write in some tag compliance with NFC Type 5 Tag.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		April-2021.
*******************************************************************************/

#ifndef NFC_T5T_H_
#define NFC_T5T_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "mcu_includes.h"
#include "nfc_common.h"
#include "nfcv.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Driver handle states.
 */
typedef enum {

	NFC_T5T_HANDLE_STATE_START,				/**< Process start. */
	NFC_T5T_HANDLE_STATE_WAIT,				/**< Wait the response. */
	NFC_T5T_HANDLE_STATE_READ_BLOCK,		/**< Block read. */
	NFC_T5T_HANDLE_STATE_WRITE_BLOCK,		/**< Block write. */
	NFC_T5T_HANDLE_STATE_STOP,				/**< Process stop. */
	NFC_T5T_HANDLE_STATE_ERROR,				/**< Error handling. */

} nfc_t5t_handle_state_t;

/**
 * Driver register.
 */
typedef union {
	
	uint8_t value;							/**< Value. */
	struct {
		uint8_t enable		: 1;			/**< Enable request. */
		uint8_t block		: 1;			/**< Block request. */
		uint8_t rdBlockRqst : 2;			/**< Read block request. */
		uint8_t wrBlockRqst	: 1;			/**< Write block request. */
		uint8_t on			: 1;			/**< On/off indication (application usage). */
		uint8_t reserved 	: 3;			/**< Reserved. */
	} flags;

} nfc_t5t_handle_reg_t;

/**
 * Driver handle.
 */
typedef struct {

	nfc_t5t_handle_reg_t reg;				/**< Register. */
	nfc_t5t_handle_state_t state;			/**< Main state machine state. */
	nfc_protocol_control_t protocolControl;	/**< Protocol control. */
	nfcv_handle_t *nfcv;					/**< ISO15693. */
	uint8_t rdIdx;							/**< Block index. */
	uint8_t rdBlock[4];						/**< Read block. */
	uint8_t wrIdx;							/**< Write index. */
	uint8_t wrBlock[4];						/**< Write block. */
	uint8_t *rdBuffer;						/**< Read buffer pointer. */
	uint8_t *wrBuffer;						/**< Write buffer pointer. */

} nfc_t5t_handle_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

/* Core: */
void Nfc_T5T_Init(void *param, void *iso);
void Nfc_T5T_Tick(void *param);
void Nfc_T5T_Handle(void *param);
void Nfc_T5T_Control_Set(nfc_protocol_control_t control, void *param);
nfc_protocol_control_t Nfc_T5T_Control_Get(void *param);

//----------------------------------------------------------------------------//

#ifdef __cplusplus
}
#endif

#endif /* NFC_T5T_H_ */

//----------------------------------------------------------------------------//
