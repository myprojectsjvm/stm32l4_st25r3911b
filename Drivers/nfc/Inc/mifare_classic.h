/***************************************************************************//**
	@file		mifare_classic.h
	@brief		MIFARE Classic driver.
********************************************************************************

	This driver can be used to authenticate, read and write in the MIFARE
	Classic.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		December-2020.
*******************************************************************************/

#ifndef MIFARE_CLASSIC_H_
#define MIFARE_CLASSIC_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "mcu_includes.h"
#include "nfc_common.h"
#include "nfca.h"
#include "crypto1.h"
#include <string.h>

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/**
 * UID size.
 */
#define MIFARE_CLASSIC_UID_SIZE			4

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Driver handle states.
 */
typedef enum {

	MIFARE_CLASSIC_HANDLE_STATE_START,			/**< Process start. */
	MIFARE_CLASSIC_HANDLE_STATE_WAIT,			/**< Wait the response. */
	MIFARE_CLASSIC_HANDLE_STATE_AUTH,			/**< Authentication procedure. */
	MIFARE_CLASSIC_HANDLE_STATE_READ,			/**< Block read. */
	MIFARE_CLASSIC_HANDLE_STATE_WRITE,			/**< Block write. */
	MIFARE_CLASSIC_HANDLE_STATE_STOP,			/**< Process stop. */
	MIFARE_CLASSIC_HANDLE_STATE_ERROR,			/**< Error handling. */

} mifare_classic_handle_state_t;

/**
 * Key types.
 */
typedef enum {

	MIFARE_CLASSIC_KEY_A = 0x00,				/**< Authentication using key A.  */
	MIFARE_CLASSIC_KEY_B						/**< Authentication using key B.  */

} mifare_classic_key_t;

/**
 * Driver register.
 */
typedef union {
	
	uint8_t
	value;										/**< Value. */
	struct {
		uint8_t enable		: 1;				/**< Enable request. */
		uint8_t block		: 1;				/**< Block request. */
		uint8_t rdRqst 		: 1;				/**< Read request. */
		uint8_t wrRqst 		: 1;				/**< Write request. */
		uint8_t on			: 1;				/**< On/off indication (application usage). */
		uint8_t reserved 	: 3;				/**< Reserved. */
	} flags;

} mifare_classic_handle_reg_t;

/**
 * Authentication parameters.
 */
typedef struct {

	uint64_t key;								/**< Key. */
	uint8_t type	: 1;						/**< Authentication type. */
	uint8_t block	: 7;						/**< Desired block [0 to 63]. */

} mifare_classic_access_t;

/**
 * Driver handle.
 */
typedef struct {

	mifare_classic_handle_reg_t reg;			/**< Register. */
	mifare_classic_handle_state_t state;		/**< Main state machine state. */
	mifare_classic_access_t access;				/**< Access parameters. */
	uint8_t *rdBuffer;							/**< Read buffer pointer. */
	uint8_t *wrBuffer;							/**< Write buffer pointer. */
	nfc_protocol_control_t protocolControl;		/**< Protocol control. */
	nfca_handle_t *nfca;						/**< ISO14443-A. */
	crypto1_state cryptoState;					/**< CRYPTO-1 state. */

} mifare_classic_handle_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

/* Core: */
void Mifare_Classic_Init(void *param, void *iso);
void Mifare_Classic_Tick(void *param);
void Mifare_Classic_Handle(void *param);
void Mifare_Classic_Control_Set(nfc_protocol_control_t control, void *param);
nfc_protocol_control_t Mifare_Classic_Control_Get(void *param);

//----------------------------------------------------------------------------//

#ifdef __cplusplus
}
#endif

#endif /* MIFARE_CLASSIC_H_ */

//----------------------------------------------------------------------------//
