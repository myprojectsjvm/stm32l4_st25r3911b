/***************************************************************************//**
	@file		nfcv.h
	@brief		NFC-V commands.
********************************************************************************

	This driver is used to search for tags compliance with ISO15693.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		April-2021.
*******************************************************************************/

#ifndef NFCV_H_
#define NFCV_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "mcu_includes.h"

#include "nfc_common.h"

#include "iso15693_3.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Driver control.
 */
typedef enum {

	NFCV_CONTROL_NONE,						/**< Wait for some command. */
	NFCV_CONTROL_START,						/**< Command to start the process. */
	NFCV_CONTROL_STOP,						/**< Command to stop the process. */

} nfcv_control_t;

/**
 * Driver handle states.
 */
typedef enum {

	NFCV_HANDLE_STATE_STANDBY,				/**< Wait to start. */
	NFCV_HANDLE_STATE_FIELD_ON,				/**< Turn on the field. */
	NFCV_HANDLE_STATE_INIT,					/**< Protocol initialization. */
	NFCV_HANDLE_STATE_GUARD_TIME,			/**< Waits the minimum time after the field is turned on. */
	NFCV_HANDLE_STATE_INVENTORY,			/**< Inventory. */
	NFCV_HANDLE_STATE_WAIT,					/**< Wait to stop. */
	NFCV_HANDLE_STATE_DEINIT,				/**< Protocol deinitialization. */
	NFCV_HANDLE_STATE_ERROR,				/**< Error handling. */

} nfcv_handle_state_t;

/**
 * Driver register.
 */
typedef union {

	uint8_t value;							/**< Value. */
	struct {
		uint8_t error		: 1;			/**< Error flag. */
		uint8_t reserved 	: 7;			/**< Reserved. */
	} flags;

} nfcv_handle_reg_t;

/**
 * Driver handle.
 */
typedef struct {

	nfcv_handle_reg_t reg;					/**< Register. */
	nfcv_handle_state_t state;				/**< Handle state. */
	nfcv_control_t control;					/**< Process control. */
	iso15693ProximityCard_t cards[5];		/**< ISO14443A card structure. */
	uint8_t cardsFound;						/**< Cards found. */

} nfcv_handle_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

/* Core: */
void Nfcv_Init(nfcv_handle_t *h);
void Nfcv_Handle(nfcv_handle_t *h);

//----------------------------------------------------------------------------//

#ifdef __cplusplus
}
#endif

#endif /* NFCV_H_ */

//----------------------------------------------------------------------------//
