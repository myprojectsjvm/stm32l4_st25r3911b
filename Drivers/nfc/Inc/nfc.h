/***************************************************************************//**
	@file		nfc.h
	@brief		NFC driver.
********************************************************************************

	This driver is the last layer that interfaces the NFC drivers with the main
	application.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		December-2020.
*******************************************************************************/

#ifndef NFC_H_
#define NFC_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "mcu_includes.h"
#include "nfc_common.h"
#include "nfca.h"
#include "nfcv.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Handle states.
 */
typedef enum {

	NFC_HANDLE_STATE_OFF,			/**< NFC is off. */
	NFC_HANDLE_STATE_WAIT,			/**< Wait for some event (polling time or wake-up detection). */
	NFC_HANDLE_STATE_START,			/**< Protocol start procedure. */
	NFC_HANDLE_STATE_RUN,			/**< Protocol running state. */
	NFC_HANDLE_STATE_ERROR,			/**< Error state. */

} nfc_handle_state_t;

/**
 * Driver error code.
 */
typedef enum {

	NFC_ERROR_OK = 0x00,			/**< Successful indication. */
	NFC_ERROR_FAIL,					/**< Failure indication. */

} nfc_error_t;

/**
 * Driver register.
 */
typedef union {
	
	uint8_t value;					/**< Value. */
	struct {
		uint8_t on			: 1;	/**< Turn on flag/off command. */
		uint8_t wakeupRqst	: 1;	/**< Wake-up request (must be activated while the NFC is off). */
		uint8_t wakeupMode	: 1;	/**< Wake-up mode. */
		uint8_t reserved 	: 8;	/**< Reserved. */
	} flags;

} nfc_handle_reg_t;

/**
 * Driver handle.
 */
typedef struct {

	/* Internal control: */
	nfc_handle_reg_t reg;			/**< Register. */
	nfc_handle_state_t state;		/**< Handle state. */
	uint32_t counter;				/**< Generic counter. */

	/* Settings: */
	uint32_t period;				/**< Polling period (x10ms). */

	/* Protocol control: */
	const nfc_protocol_t *protocol;	/**< Protocol pointers. */
	uint32_t n;						/**< Number of protocols. */
	uint8_t index;					/**< Protocol index. */

	/* NFC protocols: */
	nfca_handle_t nfca;				/**< ISO14443-A. */
	nfcv_handle_t nfcv;				/**< ISO15693. */

} nfc_handle_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

/* Core: */
nfc_error_t Nfc_Init(nfc_handle_t *h, const nfc_protocol_t *protocols, uint32_t n);
void Nfc_Tick(nfc_handle_t *h);
void Nfc_Handle(nfc_handle_t *h);

//----------------------------------------------------------------------------//

#ifdef __cplusplus
}
#endif

#endif /* NFC_H_ */

//----------------------------------------------------------------------------//
