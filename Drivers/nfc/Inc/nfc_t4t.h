/***************************************************************************//**
	@file		nfc_t4t.h
	@brief		NFC Type 4 Tag driver.
********************************************************************************

	This driver interfaces with the NFC layer to provide the identification,
	read and write in some tag compliance with NFC Type 4 Tag.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		March-2021.
*******************************************************************************/

#ifndef NFC_T4T_H_
#define NFC_T4T_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "mcu_includes.h"
#include "nfc_common.h"
#include "nfca.h"

#include "rfal_t4t.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/**
 * NFC Type 4 Tag maximum command.
 *
 * @attention Must be greater than 255+8.
 */
#define NFC_T4T_CMD_MAX			263

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Driver handle states.
 */
typedef enum {

	NFC_T4T_HANDLE_STATE_START,				/**< Process start. */
	NFC_T4T_HANDLE_STATE_WAIT,				/**< Wait the response. */
	NFC_T4T_HANDLE_STATE_APDU_STATUS,		/**< APDU (Application Protocol Data Unit) status. */
	NFC_T4T_HANDLE_STATE_APP_SELECT,		/**< NDEF Tag Application Select procedure. */
	NFC_T4T_HANDLE_STATE_CC_SELECT,			/**< Capability Container Select procedure. */
	NFC_T4T_HANDLE_STATE_CC_LENGTH,			/**< CC file length read. */
	NFC_T4T_HANDLE_STATE_CC_FILE,			/**< CC file content read. */
	NFC_T4T_HANDLE_STATE_NDEF_SELECT,		/**< NDEF Select procedure. */
	NFC_T4T_HANDLE_STATE_NDEF_LENGTH_READ,	/**< NDEF length read. */
	NFC_T4T_HANDLE_STATE_NDEF_FILE_READ,	/**< NDEF content read. */
	NFC_T4T_HANDLE_STATE_NDEF_LENGTH_ERASE,	/**< NDEF length erase. */
	NFC_T4T_HANDLE_STATE_NDEF_FILE_WRITE,	/**< NDEF content write. */
	NFC_T4T_HANDLE_STATE_NDEF_LENGTH_WRITE,	/**< NDEF length write. */
	NFC_T4T_HANDLE_STATE_STOP,				/**< Process stop. */
	NFC_T4T_HANDLE_STATE_ERROR,				/**< Error handling. */

} nfc_t4t_handle_state_t;

/**
 * Driver register.
 */
typedef union {
	
	uint8_t value;							/**< Value. */
	struct {
		uint8_t enable		: 1;			/**< Enable request. */
		uint8_t block		: 1;			/**< Block request. */
		uint8_t rdRqst 		: 1;			/**< Read request. */
		uint8_t wrRqst 		: 1;			/**< Write request. */
		uint8_t on			: 1;			/**< On/off indication (application usage). */
		uint8_t reserved 	: 3;			/**< Reserved. */
	} flags;

} nfc_t4t_handle_reg_t;

/**
 * Driver handle.
 */
typedef struct {

	nfc_t4t_handle_reg_t reg;				/**< Register. */
	nfc_t4t_handle_state_t state;			/**< Main state machine state. */
	nfc_t4t_handle_state_t next;			/**< Main state machine next state. */
	nfc_protocol_control_t protocolControl;	/**< Protocol control. */
	nfca_handle_t *nfca;					/**< ISO14443-A. */
	rfalIsoDepDevice isoDepDevice;			/**< ISO-DEP device. */
	uint8_t cmd[NFC_T4T_CMD_MAX];			/**< Command. */
	uint16_t rxLen;							/**< Received INF data length in Bytes */
	rfalIsoDepApduBufFormat rxBuf;			/**< Receive Buffer struct reference in Bytes. */
	rfalIsoDepBufFormat tmpBuf;				/**< Temp buffer for Rx I-Blocks (internal).  */
	uint8_t ndefMax;						/**< Maximum number of data to read/write. */
	uint16_t ndefSize;						/**< NDEF size. */
	uint16_t ndefIdx;						/**< NDEF index. */
	uint8_t *rdBuffer;						/**< Read buffer pointer. */
	uint16_t rdSize;						/**< Read buffer size. */
	uint8_t *wrBuffer;						/**< Write buffer pointer. */
	uint16_t wrSize;						/**< Write buffer size. */

} nfc_t4t_handle_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

/* Core: */
void Nfc_T4T_Init(void *param, void *iso);
void Nfc_T4T_Tick(void *param);
void Nfc_T4T_Handle(void *param);
void Nfc_T4T_Control_Set(nfc_protocol_control_t control, void *param);
nfc_protocol_control_t Nfc_T4T_Control_Get(void *param);

//----------------------------------------------------------------------------//

#ifdef __cplusplus
}
#endif

#endif /* NFC_T4T_H_ */

//----------------------------------------------------------------------------//
