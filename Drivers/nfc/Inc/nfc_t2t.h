/***************************************************************************//**
	@file		nfc_t2t.h
	@brief		NFC Type 2 Tag driver.
********************************************************************************

	This driver interfaces with the NFC layer to provide the identification,
	read and write in some tag compliance with NFC Type 2 Tag.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		March-2021.
*******************************************************************************/

#ifndef NFC_T2T_H_
#define NFC_T2T_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "mcu_includes.h"
#include "nfc_common.h"
#include "nfca.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Driver handle states.
 */
typedef enum {

	NFC_T2T_HANDLE_STATE_START,				/**< Process start. */
	NFC_T2T_HANDLE_STATE_WAIT,				/**< Wait the response. */
	NFC_T2T_HANDLE_STATE_READ,				/**< Block read. */
	NFC_T2T_HANDLE_STATE_WRITE,				/**< Block write. */
	NFC_T2T_HANDLE_STATE_STOP,				/**< Process stop. */
	NFC_T2T_HANDLE_STATE_ERROR,				/**< Error handling. */

} nfc_t2t_handle_state_t;

/**
 * Driver register.
 */
typedef union {
	
	uint8_t value;							/**< Value. */
	struct {
		uint8_t enable		: 1;			/**< Enable request. */
		uint8_t block		: 1;			/**< Block request. */
		uint8_t rdRqst 		: 1;			/**< Read request. */
		uint8_t wrRqst 		: 1;			/**< Write request. */
		uint8_t on			: 1;			/**< On/off indication (application usage). */
		uint8_t reserved 	: 3;			/**< Reserved. */
	} flags;

} nfc_t2t_handle_reg_t;

/**
 * Driver handle.
 */
typedef struct {

	nfc_t2t_handle_reg_t reg;				/**< Register. */
	nfc_t2t_handle_state_t state;			/**< Main state machine state. */
	uint16_t block;							/**< Block number. */
	uint8_t *rdBuffer;						/**< Read buffer pointer (16 bytes). */
	uint8_t *wrBuffer;						/**< Write buffer pointer (4 bytes). */
	nfc_protocol_control_t protocolControl;	/**< Protocol control. */
	nfca_handle_t *nfca;					/**< ISO14443-A. */

} nfc_t2t_handle_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

/* Core: */
void Nfc_T2T_Init(void *param, void *iso);
void Nfc_T2T_Tick(void *param);
void Nfc_T2T_Handle(void *param);
void Nfc_T2T_Control_Set(nfc_protocol_control_t control, void *param);
nfc_protocol_control_t Nfc_T2T_Control_Get(void *param);

//----------------------------------------------------------------------------//

#ifdef __cplusplus
}
#endif

#endif /* NFC_T2T_H_ */

//----------------------------------------------------------------------------//
