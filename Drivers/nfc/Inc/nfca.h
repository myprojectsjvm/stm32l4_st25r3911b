/***************************************************************************//**
	@file		nfca.h
	@brief		NFC-A commands.
********************************************************************************

	This driver is used to search for tags compliance with ISO14443-A.

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		March-2021.
*******************************************************************************/

#ifndef NFCA_H_
#define NFCA_H_

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

#include "mcu_includes.h"

#include "nfc_common.h"

#include "iso14443a.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Driver control.
 */
typedef enum {

	NFCA_CONTROL_NONE,						/**< Wait for some command. */
	NFCA_CONTROL_START,						/**< Command to start the process. */
	NFCA_CONTROL_STOP,						/**< Command to stop the process. */

} nfca_control_t;

/**
 * Driver handle states.
 */
typedef enum {

	NFCA_HANDLE_STATE_STANDBY,				/**< Wait to start. */
	NFCA_HANDLE_STATE_FIELD_ON,				/**< Turn on the field. */
	NFCA_HANDLE_STATE_INIT,					/**< Protocol initialization. */
	NFCA_HANDLE_STATE_GUARD_TIME,			/**< Waits the minimum time after the field is turned on. */
	NFCA_HANDLE_STATE_REQUEST,				/**< Request command. */
	NFCA_HANDLE_STATE_ANTICOLLISION,		/**< Anticollision loop. */
	NFCA_HANDLE_STATE_WAIT,					/**< Wait to stop. */
	NFCA_HANDLE_STATE_DEINIT,				/**< Protocol deinitialization. */
	NFCA_HANDLE_STATE_ERROR,				/**< Error handling. */

} nfca_handle_state_t;

/**
 * Driver register.
 */
typedef union {

	uint8_t value;							/**< Value. */
	struct {
		uint8_t error		: 1;			/**< Error flag. */
		uint8_t reserved 	: 7;			/**< Reserved. */
	} flags;

} nfca_handle_reg_t;

/**
 * Driver handle.
 */
typedef struct {

	nfca_handle_reg_t reg;					/**< Register. */
	nfca_handle_state_t state;				/**< Handle state. */
	nfca_control_t control;					/**< Process control. */
	iso14443AProximityCard_t card;			/**< ISO14443A card structure. */

} nfca_handle_t;

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

/* Core: */
void Nfca_Init(nfca_handle_t *h);
void Nfca_Handle(nfca_handle_t *h);

//----------------------------------------------------------------------------//

#ifdef __cplusplus
}
#endif

#endif /* NFCA_H_ */

//----------------------------------------------------------------------------//
