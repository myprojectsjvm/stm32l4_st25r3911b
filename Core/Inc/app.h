/***************************************************************************//**
	@file		app.h
	@brief		ST25R3911B test.
********************************************************************************
	ST25R3911B test.

	@note		STM32CubeIDE Version: 1.5.0.

	@note		STM32L476RE microcontroller.

	@see		http://www.doxygen.nl/index.html
	
********************************************************************************
	@author		Jose Vinicius Melo.
	@date		December-2020.
*******************************************************************************/

#ifndef APP_H_
#define APP_H_

/*******************************************************************************
	INCLUDES:
*******************************************************************************/

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/*******************************************************************************
	EXTERNAL VARIABLES:
*******************************************************************************/

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	PROTOTYPES:
*******************************************************************************/

void App_Main(void);

//----------------------------------------------------------------------------//

#endif /* APP_H_ */

//----------------------------------------------------------------------------//
