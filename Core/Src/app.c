/***************************************************************************//**
	@file		app.c
	@brief		ST25R3911B test.
********************************************************************************
	ST25R3911B test.

	@note		STM32CubeIDE Version: 1.5.0.

	@note		STM32L476RE microcontroller.

	@see		http://www.doxygen.nl/index.html

********************************************************************************
	@author		Jose Vinicius Melo.
	@date		December-2020.
*******************************************************************************/

#include "app.h"

#include "main.h"
#include "spi.h"
#include "tim.h"
#include "gpio.h"

#include "nfc.h"
#include "nfc_t2t.h"
#include "nfc_t4t.h"
#include "nfc_t5t.h"
#include "mifare_classic.h"

#include "st25r3911_interrupt.h"

/*******************************************************************************
	SETTINGS:
*******************************************************************************/

/**
 * Signaling (ON) some MIFARE card.
 */
#define FLAG_MIFARE_ON					HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_SET)

/**
 * Signaling (OFF) some MIFARE card.
 */
#define FLAG_MIFARE_OFF					HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, GPIO_PIN_RESET)

/**
 * Signaling (ON) some T2T card.
 */
#define FLAG_T2T_ON						HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_SET)

/**
 * Signaling (OFF) some T2T card.
 */
#define FLAG_T2T_OFF					HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET)

/**
 * Signaling (ON) some T4T card.
 */
#define FLAG_T4T_ON						HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET)

/**
 * Signaling (OFF) some T4T card.
 */
#define FLAG_T4T_OFF					HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET)

/**
 * Signaling (ON) some T5T card.
 */
#define FLAG_T5T_ON						HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_SET)

/**
 * Signaling (OFF) some T5T card.
 */
#define FLAG_T5T_OFF					HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, GPIO_PIN_RESET)

/*******************************************************************************
	DEFINES:
*******************************************************************************/

/*******************************************************************************
	TYPEDEFS:
*******************************************************************************/

/**
 * Application handle.
 */
typedef struct {

	volatile union {
		uint8_t value;
		struct {
			uint8_t t10ms 		: 1;				/**< 10 ms event. */
			uint8_t reserved 	: 7;				/**< Reserved field. */
		} flags;
	} reg;

	nfc_handle_t nfc;							/**< NFC handle structure. */
	mifare_classic_handle_t mifareClassic;		/**< MIFARE Classic handle structure. */
	nfc_t2t_handle_t nfcT2T;					/**< NFC Type 2 Tag handle structure. */
	nfc_t4t_handle_t nfcT4T;					/**< NFC Type 4 Tag handle structure. */
	nfc_t5t_handle_t nfcT5T;					/**< NFC Type 5 Tag handle structure. */

} app_handle_t;

/*******************************************************************************
	GLOBALS VARIABLES:
*******************************************************************************/

/**
 * NFC counter.
 */
uint8_t globalCommProtectCnt = 0;

/**
 * NFC IRQ flag.
 */
volatile uint32_t AppNfcIrq = 0;

/**
 * Application handle.
 */
app_handle_t AppHandle;

/**
 * NFC protocols.
 */
const nfc_protocol_t NfcProtocols[] = {
	{
		.type					= NFC_TYPE_MIFARE_CLASSIC,
		.iso					= NFC_ISO14443_A,
		.h 						= &AppHandle.mifareClassic,
		.nfcProtocolInit 		= &Mifare_Classic_Init,
		.nfcProtocolTick		= &Mifare_Classic_Tick,
		.nfcProtocolHandle		= &Mifare_Classic_Handle,
		.nfcProtocolControlSet 	= &Mifare_Classic_Control_Set,
		.nfcProtocolControlGet	= &Mifare_Classic_Control_Get,
	},
	{
		.type					= NFC_TYPE_NFC_T2T,
		.iso					= NFC_ISO14443_A,
		.h 						= &AppHandle.nfcT2T,
		.nfcProtocolInit 		= &Nfc_T2T_Init,
		.nfcProtocolTick		= &Nfc_T2T_Tick,
		.nfcProtocolHandle		= &Nfc_T2T_Handle,
		.nfcProtocolControlSet 	= &Nfc_T2T_Control_Set,
		.nfcProtocolControlGet	= &Nfc_T2T_Control_Get,
	},
	{
		.type					= NFC_TYPE_NFC_T4T,
		.iso					= NFC_ISO14443_A,
		.h 						= &AppHandle.nfcT4T,
		.nfcProtocolInit 		= &Nfc_T4T_Init,
		.nfcProtocolTick		= &Nfc_T4T_Tick,
		.nfcProtocolHandle		= &Nfc_T4T_Handle,
		.nfcProtocolControlSet 	= &Nfc_T4T_Control_Set,
		.nfcProtocolControlGet	= &Nfc_T4T_Control_Get,
	},
	{
		.type					= NFC_TYPE_NFC_T5T,
		.iso					= NFC_ISO15693,
		.h 						= &AppHandle.nfcT5T,
		.nfcProtocolInit 		= &Nfc_T5T_Init,
		.nfcProtocolTick		= &Nfc_T5T_Tick,
		.nfcProtocolHandle		= &Nfc_T5T_Handle,
		.nfcProtocolControlSet 	= &Nfc_T5T_Control_Set,
		.nfcProtocolControlGet	= &Nfc_T5T_Control_Get,
	}
};

/**
 * Global counter.
 */
uint32_t Counterus;

/*******************************************************************************
	CONSTANTS:
*******************************************************************************/

/*******************************************************************************
	LOCAL PROTOTYPES:
*******************************************************************************/

/***************************************************************************//**
	@brief		Main application routine.
	@param		None.
	@return		None.
*******************************************************************************/

void App_Main(void) {

	/* Initializations: */
	HAL_Delay(500);
	HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_SET);
	HAL_TIM_Base_Start_IT(&htim1);
	HAL_TIM_Base_Start_IT(&htim2);

	/* NFC initialization: */
	if (Nfc_Init(&AppHandle.nfc, NfcProtocols, sizeof(NfcProtocols)/sizeof(nfc_protocol_t)) != NFC_ERROR_OK) {
		/* Error: */
		while(1);
	}

	/* Test: */
	AppHandle.nfc.reg.flags.on = 1;
	AppHandle.nfc.reg.flags.wakeupRqst = 0;
	AppHandle.nfc.period = 25;

	AppHandle.mifareClassic.reg.flags.enable = 1;
	AppHandle.mifareClassic.reg.flags.block = 0;

	AppHandle.nfcT2T.reg.flags.enable = 1;
	AppHandle.nfcT2T.reg.flags.block = 0;

	AppHandle.nfcT4T.reg.flags.enable = 1;
	AppHandle.nfcT4T.reg.flags.block = 0;

	AppHandle.nfcT5T.reg.flags.enable = 1;
	AppHandle.nfcT5T.reg.flags.block = 0;

	/* Main loop: */
	while (1) {

		/* Time routines: */
		if (AppHandle.reg.flags.t10ms) {
			AppHandle.reg.flags.t10ms = 0;
			Nfc_Tick(&AppHandle.nfc);
		}

		/* NFC handle: */
		Nfc_Handle(&AppHandle.nfc);

	}

}

/***************************************************************************//**
	@brief		NFC event callback.
	@param		event	NFC events.
	@param		type	NFC types.
	@param		h		Driver handle structure.
	@return		None.
*******************************************************************************/

void Nfc_Event_Callback(nfc_event_t event, nfc_type_t type, void *h) {

	static uint8_t buffer[16];
	static uint8_t wbuffer[] = {0xD1, 0x01, 0x07, 0x54, 0x02, 0x65, 0x6E, 0x4C, 0x4C, 0x4C, 0x4C};
	static uint8_t write = 0;

	/* NXP MIFARE Classic: */
	if (type == NFC_TYPE_MIFARE_CLASSIC) {
		/* The tag was identified: */
		if (event == NFC_EVENT_IDENTIFICATION) {
			if (AppHandle.mifareClassic.reg.flags.on == 0) {
				AppHandle.mifareClassic.reg.flags.on = 1;
				AppHandle.mifareClassic.rdBuffer = buffer;
				AppHandle.mifareClassic.wrBuffer = buffer;
				AppHandle.mifareClassic.reg.flags.rdRqst = 1;
				if (write) {
					write = 0;
					AppHandle.mifareClassic.reg.flags.wrRqst = 1;
				}
				FLAG_MIFARE_ON;
			}
		}
		/* The tag was read: */
		else if (event == NFC_EVENT_READ) {
			FLAG_MIFARE_ON;
		}
		/* The tag was write: */
		else if (event == NFC_EVENT_WRITE) {
			FLAG_MIFARE_ON;
		}
		/* The tag was not found: */
		else if (event == NFC_EVENT_NOT_FOUND) {
			if (AppHandle.mifareClassic.reg.flags.on == 1) {
				AppHandle.mifareClassic.reg.flags.on = 0;
				FLAG_MIFARE_OFF;
			}
		}
	}

	/* NFC Type 2 Tag: */
	else if (type == NFC_TYPE_NFC_T2T) {
		/* The tag was identified: */
		if (event == NFC_EVENT_IDENTIFICATION) {
			if (AppHandle.nfcT2T.reg.flags.on == 0) {
				AppHandle.nfcT2T.reg.flags.on = 1;
				AppHandle.nfcT2T.rdBuffer = buffer;
				AppHandle.nfcT2T.wrBuffer = buffer;
				AppHandle.nfcT2T.reg.flags.rdRqst = 1;
				if (write) {
					write = 0;
					buffer[0] = 0x11;
					buffer[1] = 0x22;
					buffer[2] = 0x33;
					buffer[3] = 0x44;
					AppHandle.nfcT2T.reg.flags.wrRqst = 1;
				}
				FLAG_T2T_ON;
			}
		}
		/* The tag was read: */
		else if (event == NFC_EVENT_READ) {
			FLAG_T2T_ON;
			buffer[0] = 0x11;
			buffer[1] = 0x22;
			buffer[2] = 0x33;
			buffer[3] = 0x44;
		}
		/* The tag was write: */
		else if (event == NFC_EVENT_WRITE) {
			FLAG_T2T_ON;
		}
		/* The tag was not found: */
		else if (event == NFC_EVENT_NOT_FOUND) {
			if (AppHandle.nfcT2T.reg.flags.on == 1) {
				AppHandle.nfcT2T.reg.flags.on = 0;
				FLAG_T2T_OFF;
			}
		}
	}

	/* NFC Type 4 Tag: */
	else if (type == NFC_TYPE_NFC_T4T) {
		/* The tag was identified: */
		if (event == NFC_EVENT_IDENTIFICATION) {
			if (AppHandle.nfcT4T.reg.flags.on == 0) {
				AppHandle.nfcT4T.reg.flags.on = 1;
				if (write) {
					write = 0;
					AppHandle.nfcT4T.reg.flags.wrRqst = 1;
					AppHandle.nfcT4T.wrBuffer = wbuffer;
					AppHandle.nfcT4T.wrSize = sizeof(wbuffer);
				}
				else {
					AppHandle.nfcT4T.reg.flags.rdRqst = 1;
					AppHandle.nfcT4T.rdBuffer = buffer;
					AppHandle.nfcT4T.rdSize = sizeof(buffer);
				}
				FLAG_T4T_ON;
			}
		}
		/* The tag was read: */
		else if (event == NFC_EVENT_READ) {
			FLAG_T4T_ON;
		}
		/* The tag was write: */
		else if (event == NFC_EVENT_WRITE) {
		}
		/* The tag was not found: */
		else if (event == NFC_EVENT_NOT_FOUND) {
			if (AppHandle.nfcT4T.reg.flags.on == 1) {
				AppHandle.nfcT4T.reg.flags.on = 0;
				FLAG_T4T_OFF;
			}
		}
	}

	/* NFC Type 5 Tag: */
	else if (type == NFC_TYPE_NFC_T5T) {
		/* The tag was identified: */
		if (event == NFC_EVENT_IDENTIFICATION) {
			if (AppHandle.nfcT5T.reg.flags.on == 0) {
				AppHandle.nfcT5T.reg.flags.on = 1;
				FLAG_T5T_ON;
			}
			if (write) {
				write = 0;
				AppHandle.nfcT5T.reg.flags.wrBlockRqst = 1;
				AppHandle.nfcT5T.wrIdx = 0;
				AppHandle.nfcT5T.wrBlock[0] = 0x01;
				AppHandle.nfcT5T.wrBlock[1] = 0x02;
				AppHandle.nfcT5T.wrBlock[2] = 0x03;
				AppHandle.nfcT5T.wrBlock[3] = 0x04;
			}
			else {
				AppHandle.nfcT5T.reg.flags.rdBlockRqst = 1;
				AppHandle.nfcT5T.rdIdx = 0;
			}
		}
		/* The tag was read: */
		else if (event == NFC_EVENT_READ) {
			FLAG_T5T_ON;
		}
		/* The tag was write: */
		else if (event == NFC_EVENT_WRITE) {
			FLAG_T5T_ON;
		}
		/* The tag was not found: */
		else if (event == NFC_EVENT_NOT_FOUND) {
			if (AppHandle.nfcT5T.reg.flags.on == 1) {
				AppHandle.nfcT5T.reg.flags.on = 0;
				FLAG_T5T_OFF;
			}
		}
	}

}

/***************************************************************************//**
	@brief		Timer callback.
	@param		htim Timer structure pointer.
	@return		None.
*******************************************************************************/

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {

	static uint32_t t10ms;
	static uint32_t t1000ms;

	/* 1s event: */
	if (t10ms++ == 10) {
		t10ms = 0;
		AppHandle.reg.flags.t10ms = 1;
	}

	/* 1s event: */
	if (t1000ms++ == 1000) {
		t1000ms = 0;
	}

}

/***************************************************************************//**
	@brief		External interrupt callback.
	@param		htim GPIO pin.
	@return		None.
*******************************************************************************/

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

	AppNfcIrq = 1;

}

/***************************************************************************//**
	@brief		Routine description.
	@param		None.
	@return		None.
*******************************************************************************/

void Mcu_Nfc_Irq(void) {

	if (AppNfcIrq) {
		AppNfcIrq = 0;
		/* ST25R3911B interrupt: */
		st25r3911Isr();
	}

}

//----------------------------------------------------------------------------//
